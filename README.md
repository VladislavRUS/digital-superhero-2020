Для запуска бекенда нужно сначала запустить базу данных:

`docker-compose up -d --build mongodb`

После этого запускаем бекенд:

`docker-compose up -d --build backend`

