export const ROLES = {
  ADMIN: 'admin',
  ORGANIZATOR: 'organizator',
  CONTRACTOR: 'contractor',
  PARTICIPANT: 'participant',
};
