import Noty from 'noty';

const commonOptions = {
  progressBar: false,
  layout: 'bottomLeft',
  closeWith: ['click', 'button'],
  timeout: 3000,
};

class Alerter {
  static info(text, opts = {}) {
    return new Noty({
      type: 'info',
      ...commonOptions,
      text,
      ...opts,
    }).show();
  }

  static success(text, opts = {}) {
    return new Noty({
      type: 'success',
      ...commonOptions,
      text,
      ...opts,
    }).show();
  }

  static error(text, opts = {}) {
    return new Noty({
      type: 'danger',
      ...commonOptions,
      text,
      ...opts,
    }).show();
  }
}

export default Alerter;
