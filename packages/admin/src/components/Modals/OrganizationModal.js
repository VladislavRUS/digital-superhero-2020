import React from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Input, FormGroup } from 'reactstrap';
import { Form, Field } from 'react-final-form';
import { chain, required, emailFormat } from 'helpers/validators';
import cn from 'classnames';
import { showTouchedFieldError } from 'helpers/forms';

export const OrganizationModal = ({ isOpen, toggle, organization, onSubmit }) => {
  return (
    <Modal isOpen={isOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>{organization ? 'Редактирование' : 'Создание'} организации</ModalHeader>
      <Form initialValues={organization} onSubmit={onSubmit}>
        {({ submitting, handleSubmit }) => (
          <form onSubmit={handleSubmit} autoComplete="off">
            <ModalBody className="pt-0">
              <Field name="name" validate={required}>
                {({ input, meta }) => (
                  <div>
                    <FormGroup>
                      <label className="form-control-label">Название</label>
                      <Input
                        placeholder="Название"
                        className={cn(showTouchedFieldError(meta) && 'is-invalid')}
                        {...input}
                      />
                    </FormGroup>
                    <div className="invalid-feedback">{meta.error || meta.submitError}</div>
                  </div>
                )}
              </Field>
              <Field name="email" validate={chain(required, emailFormat)}>
                {({ input, meta }) => (
                  <div>
                    <FormGroup>
                      <label className="form-control-label">E-mail</label>
                      <Input
                        placeholder="E-mail"
                        className={cn(showTouchedFieldError(meta) && 'is-invalid')}
                        {...input}
                      />
                    </FormGroup>
                    <div className="invalid-feedback">{meta.error || meta.submitError}</div>
                  </div>
                )}
              </Field>
              <Field name="address">
                {({ input }) => (
                  <FormGroup>
                    <label className="form-control-label">Адрес</label>
                    <Input placeholder="Адрес" {...input} />
                  </FormGroup>
                )}
              </Field>
              <Field name="phone">
                {({ input }) => (
                  <FormGroup>
                    <label className="form-control-label">Телефон</label>
                    <Input placeholder="Телефон" {...input} />
                  </FormGroup>
                )}
              </Field>
              <Field name="site">
                {({ input }) => (
                  <FormGroup>
                    <label className="form-control-label">Веб-сайт</label>
                    <Input placeholder="Веб-сайт" {...input} />
                  </FormGroup>
                )}
              </Field>
              <Field name="description">
                {({ input }) => (
                  <FormGroup>
                    <label className="form-control-label">Описание</label>
                    <Input placeholder="Описание" {...input} />
                  </FormGroup>
                )}
              </Field>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={toggle}>
                Отмена
              </Button>
              <Button color="warning" type="submit" disabled={submitting}>
                Сохранить
              </Button>
            </ModalFooter>
          </form>
        )}
      </Form>
    </Modal>
  );
};
