import React, { useState, useRef, useEffect } from 'react';
import { useField } from 'react-final-form';
import { Card, Button, Table, Badge } from 'reactstrap';
import { uploadFile, getReports, createReport } from 'api';
import Alerter from 'common/Alerter';
import { deleteReport } from 'api';

const ReportUploadForm = () => {
  const [reports, setReports] = useState([]);
  const ref = useRef();

  const {
    input: { value: eventId },
  } = useField('eventId');

  useEffect(() => {
    fetchReports();
  }, []);

  const fetchReports = async () => {
    try {
      const { data } = await getReports(eventId);
      setReports(data);
    } catch (_) {
      Alerter.error('Произошла ошибка при загрузке отчетов');
    }
  };

  const onChange = async (event) => {
    const file = event.target.files[0];
    const {
      data: { filePath },
    } = await uploadFile(file);

    const { data } = await createReport({
      eventId,
      name: file.name,
      path: filePath,
    });
    setReports([...reports, data]);
  };

  const removeReport = async (id) => {
    try {
      await deleteReport(id);
      const newReports = reports.filter((report) => report.id !== id);
      setReports(newReports);
      Alerter.success('Отчет был успешно удален');
    } catch (_) {
      Alerter.error('Произошла ошибка при удалении отчета');
    }
  };

  return (
    <div>
      {reports.length > 0 && (
        <Card className="shadow mb-4">
          <Table className="align-items-center table-flush" responsive>
            <tbody>
              {reports.map((report) => (
                <tr key={report.id}>
                  <td>
                    {report.approved && <Badge color="success">Подтверждено</Badge>}
                    {!report.approved && <Badge color="info">На рассмотрении</Badge>}
                  </td>
                  <td>{report.name}</td>
                  <td>
                    <Button
                      type="button"
                      className="btn-icon-only btn-white text-danger"
                      onClick={() => removeReport(report.id)}
                    >
                      <i className="fas fa-trash-alt" />
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card>
      )}
      <div className="d-flex justify-content-start">
        <Button color="dark" onClick={() => ref.current.click()}>
          Загрузить отчет
        </Button>
        <input type="file" ref={ref} onChange={onChange} style={{ display: 'none' }} />
      </div>
    </div>
  );
};

export { ReportUploadForm };
