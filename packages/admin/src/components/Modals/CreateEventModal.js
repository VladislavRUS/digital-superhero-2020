import React from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import { Form, Field } from 'react-final-form';
import { chain, required, emailFormat } from 'helpers/validators';
import cn from 'classnames';
import { showTouchedFieldError } from 'helpers/forms';
import ReactDatePicker from 'react-datepicker';
import { formatWithValidate } from 'helpers/dates';
import { positiveFloatNumber } from 'helpers/formatters';

export const CreateEventModal = ({ isOpen, toggle, onSubmit }) => {
  return (
    <Modal isOpen={isOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>Создание мероприятия</ModalHeader>
      <Form onSubmit={onSubmit}>
        {({ submitting, handleSubmit }) => (
          <form onSubmit={handleSubmit} autoComplete="off">
            <ModalBody>
              <Field name="name" validate={required}>
                {({ input, meta }) => (
                  <div className="mb-3">
                    <Input
                      placeholder="Название"
                      className={cn(showTouchedFieldError(meta) && 'is-invalid')}
                      {...input}
                    />
                    <div className="invalid-feedback">{meta.error || meta.submitError}</div>
                  </div>
                )}
              </Field>
              <Field name="address">{({ input }) => <Input placeholder="Адрес" className="mb-3" {...input} />}</Field>
              <Field name="startDate" validate={required}>
                {({ input, meta }) => (
                  <div className="mb-3">
                    <ReactDatePicker
                      {...input}
                      className={cn(showTouchedFieldError(meta) && 'is-invalid')}
                      placeholderText="Дата начала"
                      selected={input.value ? new Date(input.value) : null}
                      customInput={<Input />}
                    />
                    {showTouchedFieldError(meta) && (
                      <small className="form-text text-primary">{meta.error || meta.submitError}</small>
                    )}
                  </div>
                )}
              </Field>
              <Field name="endDate" validate={required}>
                {({ input, meta }) => (
                  <div className="mb-3">
                    <ReactDatePicker
                      {...input}
                      placeholderText="Дата завершения"
                      className={cn(showTouchedFieldError(meta) && 'is-invalid')}
                      selected={input.value ? new Date(input.value) : null}
                      customInput={<Input />}
                    />
                    {showTouchedFieldError(meta) && (
                      <small className="form-text text-primary">{meta.error || meta.submitError}</small>
                    )}
                  </div>
                )}
              </Field>
              <Field name="cost" format={positiveFloatNumber}>
                {({ input, meta }) => <Input className="mb-3" placeholder="Стоимость" {...input} />}
              </Field>
              <Field name="description">
                {({ input }) => <Input type="textarea" placeholder="Описание" rows={6} {...input} />}
              </Field>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={toggle}>
                Отмена
              </Button>
              <Button color="warning" type="submit" disabled={submitting}>
                Сохранить
              </Button>
            </ModalFooter>
          </form>
        )}
      </Form>
    </Modal>
  );
};
