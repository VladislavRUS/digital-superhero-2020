import React, { useState } from 'react';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Input,
  Nav,
  NavItem,
  NavLink,
  FormGroup,
} from 'reactstrap';
import { Form, Field } from 'react-final-form';
import { required } from 'helpers/validators';
import cn from 'classnames';
import { showTouchedFieldError } from 'helpers/forms';
import ReactDatePicker from 'react-datepicker';
import { positiveFloatNumber } from 'helpers/formatters';
import { ReportUploadForm } from './ReportUploadForm';

const MainForm = () => {
  return (
    <>
      <Field name="name" validate={required}>
        {({ input, meta }) => (
          <div className="mb-3">
            <FormGroup>
              <label className="form-control-label">Название мероприятия</label>
              <Input placeholder="Название" className={cn(showTouchedFieldError(meta) && 'is-invalid')} {...input} />
            </FormGroup>
            <div className="invalid-feedback">{meta.error || meta.submitError}</div>
          </div>
        )}
      </Field>
      <Field name="address">
        {({ input }) => (
          <FormGroup>
            <label className="form-control-label">Адрес</label>
            <Input placeholder="Адрес" className="mb-3" {...input} />
          </FormGroup>
        )}
      </Field>
      <Field name="startDate" validate={required}>
        {({ input, meta }) => (
          <div className="mb-3">
            <FormGroup>
              <label className="form-control-label">Дата начала</label>
              <ReactDatePicker
                {...input}
                className={cn(showTouchedFieldError(meta) && 'is-invalid')}
                placeholderText="Дата начала"
                value={input.value ? new Date(input.value) : null}
                selected={input.value ? new Date(input.value) : null}
                customInput={<Input />}
              />
              {showTouchedFieldError(meta) && (
                <small className="form-text text-primary">{meta.error || meta.submitError}</small>
              )}
            </FormGroup>
          </div>
        )}
      </Field>
      <Field name="endDate" validate={required}>
        {({ input, meta }) => (
          <div className="mb-3">
            <FormGroup>
              <label className="form-control-label">Дата окончания</label>

              <ReactDatePicker
                {...input}
                placeholderText="Дата завершения"
                className={cn(showTouchedFieldError(meta) && 'is-invalid')}
                value={input.value ? new Date(input.value) : null}
                selected={input.value ? new Date(input.value) : null}
                customInput={<Input />}
              />
              {showTouchedFieldError(meta) && (
                <small className="form-text text-primary">{meta.error || meta.submitError}</small>
              )}
            </FormGroup>
          </div>
        )}
      </Field>
      <Field name="cost" format={positiveFloatNumber}>
        {({ input, meta }) => (
          <FormGroup>
            <label className="form-control-label">Стоимость участия</label>
            <Input className="mb-3" placeholder="Стоимость" {...input} />
          </FormGroup>
        )}
      </Field>
      <Field name="description">
        {({ input }) => (
          <FormGroup>
            <label className="form-control-label">Описание</label>
            <Input type="textarea" placeholder="Описание" rows={6} {...input} />
          </FormGroup>
        )}
      </Field>
    </>
  );
};

const tabs = [
  {
    id: 'main',
    title: 'Основное',
    form: <MainForm />,
  },
  {
    id: 'participants',
    title: 'Участники',
  },
  {
    id: 'tasks',
    title: 'Задачи',
  },
  {
    id: 'reports',
    title: 'Отчетность',
    form: <ReportUploadForm />,
  },
  {
    id: 'analytics',
    title: 'Аналитика',
  },
  {
    id: 'sponsors',
    title: 'Спонсоры',
  },
  {
    id: 'settings',
    title: 'Настройки',
  },
];

export const EditEventModal = ({ isOpen, toggle, event, onSubmit }) => {
  const [activeTab, setActiveTab] = useState(tabs[0].id);

  const currentTab = tabs.find(({ id }) => id === activeTab);
  const currentForm = currentTab ? currentTab.form : null;

  return (
    <Modal isOpen={isOpen} toggle={toggle} size="lg">
      <ModalHeader toggle={toggle}>Редактирование мероприятия "{event.name}"</ModalHeader>
      <Form initialValues={event} onSubmit={onSubmit}>
        {({ submitting, handleSubmit }) => (
          <form onSubmit={handleSubmit} autoComplete="off">
            <ModalBody>
              <Nav tabs>
                {tabs.map((tab) => (
                  <NavItem key={tab.id}>
                    <NavLink className={cn({ active: activeTab === tab.id })} onClick={() => setActiveTab(tab.id)}>
                      {tab.title}
                    </NavLink>
                  </NavItem>
                ))}
              </Nav>
              <div className="p-4" className="tab-content p-4">
                <Field name="eventId" initialValue={event.id}>
                  {({ input }) => <input type="hidden" {...input} />}
                </Field>
                {currentForm}
              </div>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={toggle}>
                Отмена
              </Button>
              <Button color="warning" type="submit" disabled={submitting}>
                Сохранить
              </Button>
            </ModalFooter>
          </form>
        )}
      </Form>
    </Modal>
  );
};
