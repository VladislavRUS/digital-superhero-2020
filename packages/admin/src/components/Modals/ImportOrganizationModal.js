import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Input, FormGroup, Table } from 'reactstrap';
import { getPublicOrganizations } from 'api';
import cn from 'classnames';
import Alerter from 'common/Alerter';
import Organizations from 'views/admin/Organizations';

export const ImportOrganizationModal = ({ isOpen, toggle, onSubmit }) => {
  const [isLoadingOrganizations, setIsLoadingOrganizations] = useState(false);
  const [organizations, setOrganizations] = useState([]);
  const [checkedOrganizationsIds, setCheckedOrganizationsIds] = useState([]);

  const fetchOrganizations = async () => {
    setIsLoadingOrganizations(true);
    try {
      const { data } = await getPublicOrganizations();
      setOrganizations(data);
    } catch (_) {
      Alerter.error('Не удалось загрузить данные с портала');
    } finally {
      setIsLoadingOrganizations(false);
    }
  };

  const toggleOrganization = (i) => {
    if (checkedOrganizationsIds.includes(i)) {
      setCheckedOrganizationsIds(checkedOrganizationsIds.filter((item) => item !== i));
    } else {
      setCheckedOrganizationsIds([...checkedOrganizationsIds, i]);
    }
  };

  const addOrganizations = () => {
    const choosedOrganizations = organizations.filter((_, i) => checkedOrganizationsIds.includes(i));
    onSubmit(choosedOrganizations);
  };

  return (
    <Modal isOpen={isOpen} toggle={toggle} size="lg">
      <ModalHeader toggle={toggle}>Импорт организации</ModalHeader>
      <ModalBody className="pt-0">
        {!organizations.length && (
          <>
            <FormGroup>
              <label className="form-control-label">Выберите источник импорта</label>
              <div>
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  disabled={isLoadingOrganizations}
                  onClick={fetchOrganizations}
                >
                  <img src="https://open.tatarstan.ru/bundles/uippurchases/images/logo.png" width="100" />
                </Button>
              </div>
            </FormGroup>
            <FormGroup>
              <label className="form-control-label">Или загрузите файл с данными организаций</label>
              <div>
                <Button className="btn-dark" disabled color="black" href="#" onClick={(e) => e.preventDefault()}>
                  Загрузить файл
                </Button>
              </div>
            </FormGroup>
          </>
        )}

        {organizations.length > 0 && (
          <Table className="align-items-center table-flush" hover responsive>
            <thead className="thead-light">
              <tr>
                <th></th>
                <th scope="col">Наименование</th>
                <th scope="col">Адрес</th>
                <th scope="col">E-mail</th>
                <th scope="col">Телефон</th>
              </tr>
            </thead>
            <tbody>
              {organizations.map((organzation, i) => (
                <tr
                  key={i}
                  className={cn(checkedOrganizationsIds.includes(i) && 'bg-secondary')}
                  onClick={() => toggleOrganization(i)}
                >
                  <td>
                    <div className="custom-control custom-checkbox d-flex align-items-start">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        checked={checkedOrganizationsIds.includes(i)}
                        onChange={() => {}}
                      />
                      <label className="custom-control-label"></label>
                    </div>
                  </td>
                  <th scope="row">
                    <span className="text-wrap">{organzation.name}</span>
                  </th>
                  <td>
                    <span className="max-w-200-px text-wrap">{organzation.address}</span>
                  </td>
                  <td>{organzation.email}</td>
                  <td>
                    <span className="max-w-200-px text-wrap">{organzation.phone}</span>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={toggle}>
          Отмена
        </Button>
        <Button color="warning" type="submit" disabled={!checkedOrganizationsIds.length} onClick={addOrganizations}>
          Добавить
        </Button>
      </ModalFooter>
    </Modal>
  );
};
