import React from 'react';

import { Container } from 'reactstrap';

const HeaderWrapper = ({ gradientColor = 'danger', children }) => {
  return (
    <>
      <div className={`header bg-gradient-${gradientColor} pb-8 pt-5 pt-md-8`}>
        <Container fluid>
          <div className="header-body">{children}</div>
        </Container>
      </div>
    </>
  );
};

export default HeaderWrapper;
