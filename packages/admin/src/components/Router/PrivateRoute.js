import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isLogin } from '../../helpers/localStorage';

const PrivateRoute = ({ component: Component, ...rest }) => {
  return null; // <Route {...rest} render={(props) => (isLogin() ? <Component {...props} /> : <Redirect to="/auth/login" />)} />;
};

export default PrivateRoute;
