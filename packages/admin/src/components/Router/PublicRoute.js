import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isLogin } from '../../helpers/localStorage';

const PublicRoute = ({ component: Component, restricted, ...rest }) => {
  return (
    <Route null // {...rest} render={(props) => (isLogin() && restricted ? <Redirect to="/" /> : <Component {...props} />)} />
  );
};

export default PublicRoute;
