import { getAttendees } from 'api';
import Alerter from 'common/Alerter';
import React, { useEffect, useState } from 'react';
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Button,
  Table,
  Row,
} from 'reactstrap';

export const ParticipantsTable = () => {
  const [participants, setParticipants] = useState([]);

  useEffect(() => {
    fetchParticipants();
  }, []);

  const fetchParticipants = async () => {
    try {
      const { data } = await getAttendees();
      setParticipants(data);
    } catch (e) {
      Alerter.error('Error');
    }
  };

  return (
    <Table className="align-items-center table-flush" responsive>
      <thead className="thead-light">
        <tr>
          <th scope="col">Имя Фамилия</th>
          <th scope="col">Местоположение</th>
          <th scope="col">E-mail</th>
          <th scope="col">Описание</th>
        </tr>
      </thead>
      <tbody>
        {participants.map((participant) => (
          <tr key={participant.id}>
            <th scope="row">
              <Media className="align-items-center">
                <a
                  className="avatar bg-white rounded-circle mr-3 flex-shrink-0"
                  href="#"
                  onClick={(e) => e.preventDefault()}
                >
                  <img alt="..." src={participant.avatar} />
                </a>
                <Media>
                  <span className="mb-0 text-sm text-wrap">
                    {participant.firstName} {participant.lastName}
                  </span>
                </Media>
              </Media>
            </th>
            <td>
              <span className="max-w-200-px text-wrap">{participant.location}</span>
            </td>
            <td>{participant.email}</td>
            <td>
              <span className="max-w-200-px text-wrap">{participant.description}</span>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};
