import React from 'react';

// reactstrap components
import { NavItem, NavLink, Nav, Container, Row, Col } from 'reactstrap';

class Login extends React.Component {
  render() {
    return (
      <>
        <footer className="py-5">
          <Container>
            <Row className="align-items-center justify-content-xl-between">
              <Col xl="6">
                <div className="copyright text-center text-xl-left text-muted">
                  © 2020{' '}
                  <a className="font-weight-bold text-warning ml-1" href="#" target="_blank">
                    Команда ASDF
                  </a>
                </div>
              </Col>
              <Col xl="6">
                <Nav className="nav-footer justify-content-center justify-content-xl-end">
                  <NavItem>
                    <NavLink href="#">О сервисе</NavLink>
                  </NavItem>

                  <NavItem>
                    <NavLink href="#" target="_blank">
                      Техподдержка
                    </NavLink>
                  </NavItem>

                  <NavItem>
                    <NavLink href="#" target="_blank">
                      Для участников
                    </NavLink>
                  </NavItem>

                  <NavItem>
                    <NavLink href="#" target="_blank">
                      Для организаторов
                    </NavLink>
                  </NavItem>
                </Nav>
              </Col>
            </Row>
          </Container>
        </footer>
      </>
    );
  }
}

export default Login;
