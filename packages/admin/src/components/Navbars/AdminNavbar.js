import { UserContext } from 'App';
import React, { useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';

import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  Nav,
  Container,
  Media,
} from 'reactstrap';

const AdminNavbar = ({ brandText }) => {
  const history = useHistory();
  const { user, resetUser } = useContext(UserContext);

  const logout = () => {
    resetUser();
    history.replace('/auth/login');
  };

  return (
    <>
      <Navbar className="navbar-top navbar-dark" expand="md" id="navbar-main">
        <Container fluid>
          <Link className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" to="/">
            {brandText}
          </Link>
          <Form className="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
            <FormGroup className="mb-0">
              <InputGroup className="input-group-alternative">
                <InputGroupAddon addonType="prepend">
                  <InputGroupText>
                    <i className="fas fa-search" />
                  </InputGroupText>
                </InputGroupAddon>
                <Input placeholder="Поиск..." type="text" />
              </InputGroup>
            </FormGroup>
          </Form>
          <Nav className="align-items-center d-none d-md-flex" navbar>
            <UncontrolledDropdown nav>
              <DropdownToggle className="pr-0" nav>
                <Media className="align-items-center">
                  <span className="avatar avatar-sm rounded-circle bg-white">
                    <img
                      alt="..."
                      src={user.logo || 'https://icon-library.com/images/no-user-image-icon/no-user-image-icon-26.jpg'}
                    />
                  </span>
                  <Media className="ml-2 d-none d-lg-block">
                    <span className="mb-0 text-sm font-weight-bold">{user.name}</span>
                  </Media>
                </Media>
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu-arrow" right>
                <DropdownItem to={`/${user.role}/user-profile`} tag={Link}>
                  <i className="ni ni-single-02" />
                  <span>Мой профиль</span>
                </DropdownItem>
                <DropdownItem to={`/${user.role}/settings`} tag={Link}>
                  <i className="ni ni-settings-gear-65" />
                  <span>Настройки</span>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem href="#" onClick={logout}>
                  <i className="ni ni-user-run" />
                  <span>Выйти</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

export default AdminNavbar;
