import React, { useContext, useEffect, useRef, useState } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';

import AdminNavbar from 'components/Navbars/AdminNavbar.js';
import AdminFooter from 'components/Footers/AdminFooter.js';
import Sidebar from 'components/Sidebar/Sidebar.js';

import routes from 'routes.js';
import { UserContext } from 'App';

const Admin = (props) => {
  const mainContentRef = useRef();
  const { user } = useContext(UserContext);

  useEffect(() => {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    mainContentRef.current.scrollTop = 0;
  });

  const getRoutes = (routes) => {
    return routes
      .map((prop, key) => {
        if (prop.layout === '/admin' && prop.user === user.role) {
          return <Route path={prop.path} component={prop.component} key={key} />;
        } else {
          return null;
        }
      })
      .filter(Boolean);
  };
  const getBrandText = (path) => {
    for (let i = 0; i < routes.length; i++) {
      if (props.location.pathname.indexOf(routes[i].path) !== -1) {
        return routes[i].name;
      }
    }
    return '';
  };

  const currentRoutes = getRoutes(routes);

  return (
    <>
      <Sidebar
        {...props}
        routes={routes}
        logo={{
          innerLink: currentRoutes[0].props.path,
          imgSrc: require('assets/img/brand/logo.png'),
          imgAlt: 'logo',
        }}
      />
      <div className="main-content" ref={mainContentRef}>
        <AdminNavbar {...props} brandText={getBrandText(props.location.pathname)} />
        <Switch>
          {currentRoutes}
          <Redirect to={currentRoutes[0].props.path} />
        </Switch>
        <Container fluid>
          <AdminFooter />
        </Container>
      </div>
    </>
  );
};

export default Admin;
