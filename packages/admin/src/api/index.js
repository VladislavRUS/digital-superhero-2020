import axios from 'axios';

export const API_URL = window.location.origin.replace('3000', '3001');

export const getFilePath = (path) => `${API_URL}${path}`;

const api = axios.create({ baseURL: API_URL });

// Organizations

export const getOrganizations = () => api.get('/organizations');

export const getOrganization = (id) => api.get(`/organizations/${id}`);

export const createOrganization = (values) => api.post('/organizations', values);

export const editOrganization = (values) => api.put(`/organizations/${values.id}`, values);

export const deleteOrganization = (id) => api.delete(`/organizations/${id}`);

export const loginAsOrganization = (values) => api.post('/organizations/login', values);

// Events

export const getEvents = () => api.get('/events');

export const getOrganizationEvents = (id) => api.get(`/events?organizationId=${id}`);

export const getEvent = (id) => api.get(`/events/${id}`);

export const createEvent = (values) => api.post('/events', values);

export const editEvent = (values) => api.put(`/events/${values.id}`, values);

export const deleteEvent = (id) => api.delete(`/events/${id}`);

// Files

export const uploadFile = (file) => {
  const formData = new FormData();
  formData.append('file', file);
  return api.post(`/upload`, formData);
};

export const createReport = (values) => api.post('/files', values);

export const getReports = (eventId) => api.get(`/files?eventId=${eventId}`);

export const getAllReports = () => api.get('/files');

export const approveReport = (reportId) => api.put(`/files/${reportId}`, { approved: true });

export const deleteReport = (reportId) => api.delete(`/files/${reportId}`);

// Attendees

export const getAttendees = () => api.get('/attendees');

// Open api

export const getPublicOrganizations = () => api.get('/public-organizations');
