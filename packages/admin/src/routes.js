import Login from 'views/Login.js';
import Register from 'views/Register.js';

import AdminEvents from 'views/admin/Events.js';
import AdminOrganizations from 'views/admin/Organizations.js';
import AdminStatistic from 'views/admin/Statistic.js';
import AdminParticipants from 'views/admin/Participants.js';
import AdminReports from 'views/admin/Reports.js';
import AdminSettings from 'views/admin/Settings';

import OrganizatorEvents from 'views/organizator/Events.js';
import OrganizatorContractors from 'views/organizator/Contractors.js';
import OrganizatorStatistic from 'views/organizator/Statistic.js';
import OrganizatorParticipants from 'views/organizator/Participants.js';
import OrganizatorReports from 'views/organizator/Reports.js';
import OrganizatorSettings from 'views/organizator/Settings';

import ContractorTasks from 'views/contractor/Tasks';
import ContractorSettings from 'views/contractor/Settings';

import Profile from 'views/examples/Profile.js';
import Maps from 'views/examples/Maps.js';
import Tables from 'views/examples/Tables.js';
import Icons from 'views/examples/Icons.js';
import { ROLES } from 'common/constants';

const adminRoutes = [
  {
    path: '/admin/events',
    name: 'Мероприятия',
    icon: 'ni ni-tv-2 text-danger',
    component: AdminEvents,
    user: ROLES.ADMIN,
    layout: '/admin',
  },
  {
    path: '/admin/organizations',
    name: 'Организации',
    icon: 'ni ni-briefcase-24 text-indigo',
    component: AdminOrganizations,
    user: ROLES.ADMIN,
    layout: '/admin',
  },
  {
    path: '/admin/statistic',
    name: 'Статистика',
    icon: 'ni ni-chart-bar-32 text-green',
    component: AdminStatistic,
    user: ROLES.ADMIN,
    layout: '/admin',
  },
  {
    path: '/admin/participants',
    name: 'Участники',
    icon: 'ni ni-single-02 text-yellow',
    component: AdminParticipants,
    user: ROLES.ADMIN,
    layout: '/admin',
  },
  {
    path: '/admin/reports',
    name: 'Отчеты',
    icon: 'ni ni-book-bookmark text-info',
    component: AdminReports,
    user: ROLES.ADMIN,
    layout: '/admin',
  },
  {
    path: '/admin/settings',
    name: 'Настройки',
    icon: 'ni ni ni-settings',
    component: AdminSettings,
    user: ROLES.ADMIN,
    layout: '/admin',
    hidden: true,
  },
];

const organizatorRoutes = [
  {
    path: '/organizator/events',
    name: 'Мероприятия',
    icon: 'ni ni-tv-2 text-danger',
    component: OrganizatorEvents,
    user: ROLES.ORGANIZATOR,
    layout: '/admin',
  },
  {
    path: '/organizator/contractors',
    name: 'Исполнители',
    icon: 'ni ni-briefcase-24 text-indigo',
    component: OrganizatorContractors,
    user: ROLES.ORGANIZATOR,
    layout: '/admin',
  },
  {
    path: '/organizator/statistic',
    name: 'Статистика',
    icon: 'ni ni-chart-bar-32 text-green',
    component: OrganizatorStatistic,
    user: ROLES.ORGANIZATOR,
    layout: '/admin',
  },
  {
    path: '/organizator/participants',
    name: 'Участники',
    icon: 'ni ni-single-02 text-yellow',
    component: OrganizatorParticipants,
    user: ROLES.ORGANIZATOR,
    layout: '/admin',
  },
  {
    path: '/organizator/reports',
    name: 'Отчеты',
    icon: 'ni ni-book-bookmark text-info',
    component: OrganizatorReports,
    user: ROLES.ORGANIZATOR,
    layout: '/admin',
  },
  {
    path: '/organizator/settings',
    name: 'Настройки',
    icon: 'ni ni ni-settings',
    component: OrganizatorSettings,
    user: ROLES.ORGANIZATOR,
    layout: '/admin',
    hidden: true,
  },
];

const contractorRoutes = [
  {
    path: '/contractor/tasks',
    name: 'Задачи',
    icon: 'ni ni-bullet-list-67 text-info',
    component: ContractorTasks,
    user: ROLES.CONTRACTOR,
    layout: '/admin',
  },
  {
    path: '/contractor/settings',
    name: 'Настройки',
    icon: 'ni ni-settings',
    component: ContractorSettings,
    user: ROLES.CONTRACTOR,
    layout: '/admin',
    hidden: true,
  },
];

var routes = [
  ...adminRoutes,
  ...organizatorRoutes,
  ...contractorRoutes,
  {
    path: '/icons',
    name: 'Icons',
    icon: 'ni ni-planet text-danger',
    component: Icons,
    layout: '/admin',
    hidden: true,
  },
  {
    path: '/maps',
    name: 'Maps',
    icon: 'ni ni-pin-3 text-orange',
    component: Maps,
    layout: '/admin',
    hidden: true,
  },
  {
    path: '/user-profile',
    name: 'User Profile',
    icon: 'ni ni-single-02 text-yellow',
    component: Profile,
    layout: '/admin',
    hidden: true,
  },
  {
    path: '/tables',
    name: 'Tables',
    icon: 'ni ni-bullet-list-67 text-red',
    component: Tables,
    layout: '/admin',
    hidden: true,
  },
  {
    path: '/login',
    name: 'Login',
    icon: 'ni ni-key-25 text-info',
    component: Login,
    layout: '/auth',
    hidden: true,
  },
  {
    path: '/register',
    name: 'Register',
    icon: 'ni ni-circle-08 text-indigo',
    component: Register,
    layout: '/auth',
    hidden: true,
  },
];
export default routes;
