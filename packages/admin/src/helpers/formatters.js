export const identity = (value) => value;

export const valueOrNull = (value) => value || null;

export const positiveFloatNumber = (value) => (value === undefined ? '' : `${value}`.replace(/[^(\d+).(\d+)]/g, ''));

export const capitalize = (value) =>
  !value
    ? ''
    : value
        .split(' ')
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ');

export const formatZip = (value) => {
  if (!value) {
    return null;
  }

  const onlyNums = `${value}`.replace(/[^\d]/g, '');
  if (onlyNums.length > 5) {
    return onlyNums.slice(0, 5) + '-' + onlyNums.slice(5, 9);
  }

  return onlyNums;
};
