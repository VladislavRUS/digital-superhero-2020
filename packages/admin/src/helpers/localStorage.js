import { ROLES } from 'common/constants';

export const setUserData = (data) => {
  localStorage.setItem('user', JSON.stringify(data));
};

export const getUserData = () => {
  const data = localStorage.getItem('user');
  try {
    return JSON.parse(data);
  } catch (_) {
    return null;
  }
};

export const clearUserData = () => {
  localStorage.removeItem('user');
};
