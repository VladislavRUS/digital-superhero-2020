import { isValid, format, parseISO } from 'date-fns';

export const formatWithValidate = (date, pattern, defaultValue = '') => {
  if (!date) {
    return defaultValue;
  }
  if (typeof date === 'string') {
    try {
      const parsedDate = parseISO(date);
      return format(parsedDate, pattern);
    } catch (_) {
      return defaultValue;
    }
  }
  return isValid(date) ? format(date, pattern) : defaultValue;
};

export const formatDate = (date) => formatWithValidate(date, 'dd.MM.yyyy');

export const formatDateTime = (date) => formatWithValidate(date, 'dd.MM.yyyy / HH:ss');
