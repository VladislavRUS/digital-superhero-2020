export const clearFieldData = ([name], state, { changeValue }) => {
  changeValue(state, name, () => undefined);
};

export const clearFieldsData = ([names], state, { changeValue }) => {
  names.forEach((name) => changeValue(state, name, () => undefined));
};

export const setFieldData = ([name, value], state, { changeValue }) => {
  changeValue(state, name, () => value);
};

export const showFieldError = (meta) => meta.submitFailed && !!(meta.error || meta.submitError);

export const showTouchedFieldError = (meta) => (meta.touched || meta.submitFailed) && (meta.error || meta.submitError);

export const getSubmitErrors = ({ response }) => (response && response.data && response.data.errors) || {};
