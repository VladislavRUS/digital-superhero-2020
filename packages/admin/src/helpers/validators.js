export const chain = (...fns) => (value) => {
  let result = undefined;
  fns.forEach((validator) => (result = result || validator(value)));
  return result;
};

export const required = (value) => (value ? undefined : 'Это обязательное поле');

export const notEmptyArray = (value) => (value && value.length ? undefined : 'Required');

export const float = (value) => (value && !isNaN(parseFloat(value)) ? undefined : 'Invalid');

export const emailFormat = (value) =>
  value &&
  !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    value,
  )
    ? 'Некорректный e-mail'
    : undefined;
