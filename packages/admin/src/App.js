import React, { createContext, useEffect, useState } from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';

import AdminLayout from 'layouts/Admin.js';
import AuthLayout from 'layouts/Auth.js';
import { setUserData, getUserData } from 'helpers/localStorage';
import { ROLES } from 'common/constants';
import { getOrganization } from 'api';
import { clearUserData } from 'helpers/localStorage';

export const UserContext = createContext(null);

export const App = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [currentUser, setCurrentUser] = useState(null);

  useEffect(() => {
    initializeUser();
  }, []);

  const initializeUser = async () => {
    setIsLoading(true);
    const user = getUserData();
    if (user && user.role === ROLES.ADMIN) {
      setCurrentUser({ role: user.role, name: 'Администратор' });
    } else if (user && user.id && (user.role === ROLES.ORGANIZATOR || user.role === ROLES.CONTRACTOR)) {
      await setOrganizatorData(user.id);
    } else {
      setCurrentUser(null);
    }
    setIsLoading(false);
  };

  const setOrganizatorData = async (id) => {
    try {
      const { data } = await getOrganization(id);
      setCurrentUser({ role: data.isContractor ? ROLES.CONTRACTOR : ROLES.ORGANIZATOR, ...data });
    } catch (_) {
      resetUser();
    }
  };

  const setUser = (data) => {
    setUserData(data);
    setCurrentUser(data);
  };

  const resetUser = () => {
    clearUserData();
    setCurrentUser(null);
  };

  if (isLoading) {
    return null;
  }

  return (
    <UserContext.Provider value={{ user: currentUser, setUser, resetUser }}>
      <HashRouter>
        <Switch>
          <Route path="/auth" component={AuthLayout} />
          {currentUser && <Route path="/" component={AdminLayout} />}
          <Redirect to={currentUser ? '/' : '/auth/login'} />
        </Switch>
      </HashRouter>
    </UserContext.Provider>
  );
};
