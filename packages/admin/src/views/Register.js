import React from 'react';
import { Link } from 'react-router-dom';

import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from 'reactstrap';

class Register extends React.Component {
  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardHeader className="bg-transparent pb-5">
              <div className="text-muted text-center mt-2 mb-4">
                <small>Зарегистрируйтесь с помощью</small>
              </div>
              <div className="text-center">
                <Button
                  className="btn-neutral btn-icon mr-4"
                  color="default"
                  href="#"
                  onClick={(e) => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="vk"
                      src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAYFBMVEVRgbj///9He7W2x95Of7eetdQ+drNCeLRKfbZGerXd5fA8dbK8zOFljr9Yhrvh6PGswNrz9vqDosrt8fdrksGAoMhfir3M2OjD0eRyl8OSrc+kutZ5m8XT3euOqs7n7fTOT9WeAAAKnUlEQVR4nOVd7ZqyOgyEUtuir1+IiqJ4/3d5CrqKQpPgAl1y5tmfC3Rsm6RtMg3C77C9zY9ZtMnPRbK/nNLlYWUR9IXyZYdlerrsk+Kcb6LsOL9tv2xp0PH/b9dolqSBVjoWsTDGSCOlDOxf36jeat9uv2G/FNsvBmkyi463wRjujptkFVtiQg7Ah8ZZCktVr5LNcdczw3WUGBUbP8SakCZWIonWPTHcZoXRwlO3uWH7U5siw2cnxjC7aPFn+u4TJctL9huG67MSxjcNBEaoMzhcAYbXVP11encYlV6/YHhd6r86OJuQeunk6GB4O6np8Csh1cnhQNoZ5hMZn3UYlZMZ7pbCd3O/Qnxo68YWhtnEBugLUrW4jibDfOG7ob/AojlSGwwL7buVv4IuMIbJNKfgC2IPMyymTtBSLCCG+bSH6B06dzPMpmxkXlhkLoY75bttPUHtHAyXU/WDn5CHdob59K3MD0TexvDGZYyWULcWhqfpBdtumFOT4ZVTF9pOvDYYsjEzd8jlJ8MrB19fh75+MEx5daHtxPSd4ZrXLCyh1m8Mz5wM6R3m/MaQXxfaTqwzzPiEMy+IrMbwwm+Q2mF6eTHccnMVd+jtkyHLQfozTCuGBTdneIcpngz/7PHZ7yDND8M1z2loJ+L6wTDiOQ3tRIweDBOeg9QO0+TBkOk0fEzEgNEWWxPlpptleIx9N2QwxMeK4YZjyHaH2VQM2Rqau6mxDPtLuPt7WFUM+U5DOxFLhjeuEU0JfbMMGZvSypgGfGO2EjZuC8IZa4Yzy5Cxs6jcRcBvL7gOmVqGAWuGll/I2VlYdxEGW74rixJqG7B2+KXLD+acHb51+fPgyNkdWod4DDLmfZgFrIO2MmwLGK/wS5hNkDNnmAcMT3/rMLOA6anMD2QRJMz7MAn2zPtwH1yYM7wEJ+YMT0Hquw0DIw3IGXtGNIGdWsmWZ56gffibz9ZbsAwOpP+W+jDbRA3MDuDaSxXNR57YFISVqVlc8rZHY2qsKQ8BaU9fGld53xXYBFkhBdc79Nv67KrzPVMXDCsaQ+2u0dw6a6A1WmuNpXzqyP0s1UISGa6AZh4dzXymPwKAd8HEGXj0H3Gc0hg+8qcccNgq81mA1AJ4rzaGHqWu3Gkna3BjHWtoKXGGYNgvZtCj/a5rX0nTrXB8S/xDGYInCgo0VD1v1Quwna6e0Kh4BcSwShVxg+hOqdCgoIjLJErIQKEM1fybb36LGJZmcJlEAXcDyFCewCf7Pk8yjdrTNzgrpjQiWgEwBFywxbbvOkLELt5c37tnB37DEDFu/e8uwRPRfUIXb75kqI/ggz3bGQsBt9SZZIx0vpMh0oWb/jd5sRDMGQdr0CI6GY7ehZj7dQcnpl2sAmGIzcIh9ukFEOaHQHkmbPRdDBVsSAc58sSGqWsxA1tTB0PEFxbDbIAq2Jq61lCPooduDBfgynKocl54Qtm4xsUQ+mXaGRo4FBqqnFciqmCRw5yCxrSdIRyxO0fLrwGHUU4L3p0hvC50DpbfA1uzO0x4Z4ZIpDekagDiErftw7QzQzha3w2ZOoLZGsek6sgQcUuDHrNgC4V2R9yVIeyVhjMzFbCNl9ZO7MgQMTNDF78guxKthZpd+xD8wuBH8kjE32rnujFcgEuR+eAKQeg2dotP7MQQGaO0U6RfAV7t2ZVp02N0YghPg3yE9C0DB/1h2Dym6cIQibhHUbGCNzHDMGsYmw4MkU2dcdRX0E5s7EnRGSLvbpkBgwDrxIaECJ0hvGk1WpUksvy2LuvDeJAZwpNwxJICbBEVxu9NoTJUcMA01hgN0G2whrEhMoxhTzhqJS92FvGxKUVjGCNnOCOrWCEM38NTEkPskGrEMVoCcVth+K9OEWT4OKpuyqq+Yxxf/4IEMwhK1OVuQIbzcnrJBfaTjRCPvgPMAqlQaxEcyqaxiZ3ZSD/wUD+4wG7RqC1zkGD9XKD5DMOvmZrAk4Gi51TEliM4vKSHoh4jLH6sH577hcCP6Ci2KWVxeTQMOT3G4anyzKDGJjw9PMG3d/w84K3UHImTS+yrXx/NqfmrDN+keh3YKGFQX4fCW30kkrtQYZuf8dw2DCNHbDXgWWs9IfVV82IIGbL9wFu1OZSj3Ct23oSs1C8dAZ3iEPkzFIw3Tr31IrpS7I+ir1vRkBSUHrH11IsUv98XRU+9iBwWcaAIZ6D1ip2funppxnIZYXiLvfQius2PYLuj/0RzPzosMZKEAuK6UloHM6pJPvq5CGeBnO5DWJTjTprFnhjGR37WUt9vVDyXt0YTgwdy3WG/kN9am1pGepzSXrL3YlHNt46/vo9mYprjOXhZLqKH3w68nwHTdjy2nmIbsAaTxhA/obnD010cjSvrvmEYCNJY8HR5WvwNxcaZC21Ge5J6/GagNpsqVxST6knq4oslf1sGLcX1+FotGtLvjzCkUfRx4lY1TnQ8SmudUDIgUJx52ieWi2573I6KEspYGP3k+wc0lwYzJG2O0L1i32reZtUhDneZfYrR+kwvc4Coi9EFxOALZIjoVNxBm4kDMLTOf0ntRrfrjvHRTiuVHYRhIBVxCw6qVkdfQbvDaUXUGOoKY0hGFdRUQAc7RXVAHug6Ud0g4wNhcwMMMBdYwgdFokYuh9P6kvqA56SAq9kFsiS+UmxNOqRem4yDDey7kSmCUKQwlKeBNfeEToDBOkdshVTgSKdIr8rL4LqJMhbFsb0nd/jTC+icmXKQIfcjaF9KIxbLWfbpI+dnyvpAn1yJO+sVpW9MMpJ+qRRC68PlnEfRv3/RJi9SoWk/rVHLmX3mA9Es1TQpvWJUDVpjTHddwPLXiT9B1SQsNWj56wjz14Lmr+fNX5Odv64+/7sR+N9vwf+OEv73zPwP7grif98T/zu7+N+7xjpsq+7O43//IWuXX91hyf8e0v/BXbKM3cXjPmDG+xiPO50ZG9PHvdz871YfXCLMG6pi3pIhW1NTCdeXDNnGbZU0cMlwzTWqqSrlK6ESphPxXlNfMRxIetg37rcBVAydkurThsieDGl5G5PDXezgLhh04ThMH3lVd4Ysh+l9kD4YhhwDNxXWGTK8jfRHnOTB0FMNw5BQ6zeG/PaFnzpIPwz7vl3IO57qh095uZFV7YbGS/zwyfDKaya+KrBfEoFDXargBbViuhfDoS7G8IKa2ENN5tGPbNggELXq67qQJRtj81avUWfIZtPt7Z6KNzFST6WLfeM9QfxdbjXn4Pf1uwTCh6BsMX1rIz5qUT4lc5OpU2wUJDdEgYtpD9Rm7VxT9jifsrlZNGVIWoSdMzVVvyhVSw1Lm3T1bjnNyRgf2hRS2sW5czW9MNyodqEch/z47jSxoSrVySFx4xRYvy5pBQ1/AlIvnSVSgIT8NZ3IWDUqBUrAQJH89VmJv07SCHUGFX+xawCyi+5U4DIqpBT6ghU5YgzDcJsVRgtfyoxOlOxMkeHaBDjDEusoMSr+M30pTaxEEtG00GkMS+yOm2QV67KoylOH2u8KYVuwSjZHuogxneEdt2s0S9JAK0s1FsYYaUrCA3Cu3mrfXha82W/ZLwZpMouOXaXTujL8wfY2P2bRJp8Vyf5ySpeHlUVv7MqXHZbp6bJPilm+ibLj/PatYtp/pvF/DuqqQk8AAAAASUVORK5CYII="
                    />
                  </span>
                  <span className="btn-inner--text ">ВКонтакте</span>
                </Button>
                <Button className="btn-neutral btn-icon" color="default" href="#" onClick={(e) => e.preventDefault()}>
                  <span className="btn-inner--icon">
                    <img
                      alt="gosuslugi"
                      src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANYAAADrCAMAAAAi2ZhvAAABLFBMVEX////vQFgUZqwAXajuMU3vPlb4vMIAYaoAW6fuLUoAX6nvOlMAWKbuJ0buMk4NZKv71dlJgrrq8ffwRl3vS2H4tLuvxd35wsj60NX0go8eZKctYaKbTnifTXaqS3K0Sm67SGvVRGHbQ186X51FXZlQW5RbWZBlV4xtVol0VYZ7U4SCUoGJUX6QUHzDR2nJRmbQRWPiQlz97O72pa7+//+dtdTxX3H0i5f1lJ/84uXycIDwVWlnk8KDp833rrbN2+qIqc7b5fDze4nD1OYuc7JRhrz96etznMjE1OY8e7esO2btFz3xXXC0x94nb7Hk7PUAU6SZqcdTaJ+Pmbpqc6KFia+Ce6SPep2Sb5WhhKOSX4izlKzForWUO2zXuMXiytPZobHTgpXOZn/ITWzmpMOmAAASG0lEQVR4nO1daXfrthGlRMkEd5m2tqZbkrZJ07y0oShLlrXbsqz42U66vbZpuv7//1AuAAmQA2ojRdqn94PPkUgCuJjBbABlQcgTjjV7mk/G55vnqovNarye929fnFz7zBnOw/y8qiFRkkyzGsA0JUlEmrJZt16KHt5BsPrnCCmETgwuOxFV1w9FD3Jf3K40HqUICjLXs6JHujucfhVt5RRITdHuWkUPdzdYc03ZiRNmJkr98lsQayLuQyoQmTS3ih53Kl7WXCuRTgxNykvsZaxJ+3PCxLSSErPW6FBSPhSxhKpoTQ6WFCWxebmMhzU5TlKhxJQSEXPmaJv1c2OmAKaZalJMRekXTQejZaZZPzdM0jQ/xnUxHp/fKRpSUkRritUyOOinZ5E/Rgmh88nTwqE1y3l56K9QitKa6PmpMDoBHp75YZLpBbO8pTKbpz2pbYqMFRcrjTs0SVs9pS//2zu+8TS1VVGJi7XmD0vSxovtLcxWfF2UtEkRRtHpi0eS8rBImRrFPL1RfKqKPP1zSe2hQGkRF7o77RJ72WhcSaH1njFQCjFzrxk6Em70x5fU+oBxLPjEJHSqJdbirvP9JUXg2lReo4p0Cvc8u+OJar81FQffWZgody9mjbmda6sdrR8Psw2/7XWumsjVv2xm9JavCUp+mrjY8HpFd7fZdPHEq1nlF3bMOTpiKlnG3H2Jk+RIWh7eecaL1CWUberncPPs7L2zs+aJShtnXoF44dklM+M48cnkaAbaHGn+YMw2CO5PrGYnMGcMh0puVpxbvvdkwjFndrb+iZOjS+I8mw5g8DIExcxCYA5H0fNYVCy4rj8Dgd2a4JyZJ8kZXOsLElOOXGG8VSWJJ8rw+nBY466wIxp94IhKW52srGyt4IkVnw81wa6vglusZhQp7YbbKuhczAODjgUcVmTtErfDDTvAFYbOD9CZPtiWaypy8b/pWNyB3lkS93Wb1gpuCBVUIYdNh+tk9tKcBwlu5RCxZ4MXOClSnvdIV/qgrcgzldsBLTDqMLWdFXEMKaCJTmfVYbgLAxKYNt/paWcDGVQJlWB7Bq45oF1cs/UMPlq0qAJY55AiieOtDzp3ACtTK4GoAoB+Z6u8QFbipkSnyV42QJSwjdcmycrccU2eDPNvk7zSI6lx0loo1QLCinQ8VJOTn2bnk/4q73rqYYDyJY07+wuAVdGb0xy0EpbDrHLm30kcmZD2CU1Oi6QfklbwneP4jWhVQgUkcBKxEOyFZjEVLJ0FjGMd42UiKGToizFWJV1WESYxXqAa3tLuwFSkV3A4OC4vBEmiXxVDPBdyKGJvjGMapkCjdqwQJx/ggYhFRcqk6AFlAytmvVFpPdJ+eGDtt3RMWbRMmLBhrPZGxCU8M7GRtD2lfB2IqeGbEdeKMRtvZnW9ME4Z9l2vEazVEEtTdzkSDlM/NO+KHk9WmDPi4ufJrwwWs7qkedHjyQpr2hiaz0UPJyssGHGht6KFwoY2Gqfar88ffdpo8Io1cTjWy2Lx4vD9nLN4eGi1nm5nVkG+0KIjKFP0RjFrAQjTZ+d2vVE0H9Id+M6I1V+JGkJuTo2QJp1PuO+V5AlGC5FXsNggMQENB8JOX0Lhq+um6X6I660zQSJVYHTv0Yrwh4wWKnP3m3Nga0ULCjQvidM5JtowFYFFNbGBUcgG34y2hf7iAmjhAMSC3jeTniklW0D7ngWwEhx6dv3hA7RwuAhsC1UZQ2NJyWfBqlb+uKNGYpogLRP5AmkRwUoKQtTPKWjh4aDIu3u/GOEvwqJ8PFNqF62IlqQQ4PNfJJ1G49bDbPbQIkdQzHPclEUqpiZSNuvJevWsoG8LKgbPaVpeAQrTkub9EL5VIEX5SKv6mJeGrQaJnKU78ssXzqIoD8+YQi98wrTEeM0TD5ouemDvQDI1LE7zrgQJ6RNtM8RbPi38PaJ+g6QVPIvrBSTb1spQrmcsvDfvPFpVk9E4DzhSNjf+p9uAZDkSASaI94JdDi0LKyuiv8O0FP8T3joqR2mOSSW9TJJDC6sYKwuswEF1ZxIYHz9UKRwOQ2vNpYU3yLHCYWDfFez7YVdRkrfwaZPhKRCPFpbWOf0lDmqDjQlMqyQVLLpo7Q16Cy1GWs/VJK1ijxmGoFMTLyjcRwmrNK11qZSQrVk7XJOhhcQjmPTaKhctpvqkcGmFFpP+Eht9P6sOw5By7GwyNVCNS8vBVk+jvmP9Fo45zB0rIjmDOYDhxhDcKCO8IwRrRnC8ElRECgcTFGoLLi28BhH16kULq10QE1q4oYISxxgexB9FQHxaJDaidCwWweOP5nMZxDVDNK0ZlxY5hxeVXOYiq5dEnaXoZS5nNinIjTG0xAcurbA8gFZednzbIqdkQ/mFRUcTSePJer1emRpSCgroF+ijCGm0olqGiBASo1pGmIGtQ6Nq4l9xqha2v2QxtJ74tIRzsPKkRImIU4UqbsX4MUujaH2URsuC3qCR6BR/Af5y1QnJRHAYWv0UWoKV/DEmdM6YvUU1SQwV8lOlDvo0wkffCRvFXxcaeCytr6CoxGlKKPGrBM5cZAvakvhtIVrofErjO6E1CQA7H+dpfadpGkLun80EkoPVP3evi6KiiO5N1XG/oM3AGK3tcKyFi5RDhs7i4anV7z/NFgU65t//gsIfihtHxvg/rdeEP/6Swhul9aeiR5MZ3iitP/+Kwhul9eeiR5MZ3iqtzyj8n1bJ8eGzz0PsR6vROWs2z9oNO6+hHYMPn1P4IDTaAdibzjDCL66HN7pcqxlGrSbrxqjXCK/YZxCaPvd73Dg7EW26cfDpBtXuPefBdFoDvebhsUnfc//ofymr5PNIN+qVCKrxOAz7Cu6N4dHnfRZc05fMCAx8y7X3AXpaH1DPTqMH1eCyDNL6NYUPwrXuD7R+Rd/TNILR47Gf6WolBj0UwFktfs2DHIgzaKdi0I1fy0GPl/4nHXhY9wi3g3YNar4vgqlVBQAfvohYffFBEG6Ce3VK2MJIpcc2TXatDsJ7p0YKrW7QUI3WG/yA0ePRUv0p3pPWX76g8FdB6DG9BMAKFzzfAHquRaull0arE0hGHSXnTG/waAWTsC+t30TwaN1jnahEt5DBDOjG/G881BlhEYHAtMjDkc4KNu7vQuDQqteFI2n9xqUlXNWZgbgYYh3seB/OZMJJV0eDQXd0ocuPlGnDk++aER91ltYQ60K08PFwyboltNQQeu8QWn/7ksL3QrjmKQlU6Mcx60pteU2ud+ilgtemOuz5iE0SsUg34f1LZt0SWhe9YQg7E1qCTMs+Gorqm2Ub90vrHQM8Bzr+2GVHTVjL4ZzU2N4w7ctEu3vTevfu3ZfvvL/v3n1Pzx9xyVhxav5nYr/BtoRwqYTX47SmrK8I1y35nBWtdzT+TvdEzNUlnuCAo5ourIbMalmclo2lQyzSIHY9D1o/9mnF9Ai7y0AHiUUwmlBbUe/hnMRphT6wI1B9ETuYHa0ffkwhoDVkho51EI+DiK7DoYXdVijNBC3Cu+t/wpphhLFXHrR+EtBiA6iACNEa7GyjJR8DphFa8AStsAXmcthcZrR+QuEfAsWkInsBFA4qyPRvo3UTk2aS1oAOoGIrMTNaNk3rB0yLDtNia3obLWz/ZRJSJmmRefJWH7arlHfOiNb9Dz+lgGnZVFAd8AjXtJpO6564NfJFklaoC3YUD0ZRSka0rr+hWH3zT/xtFH6S1npsU/Qwgc7r71No9cIAyk5GvhnR+idIKwqg4vOJg6FaG2hLSBpCiNZ1uKDw3TLV2FZaVGaRSutnEUJaAokl7JgbCi1dD2hLANwaQItMjX4f2JcoTEujFQ9H9qEVrhg8GpVEquF8EnGMgLaE0JPL8YYYWjjZrl+piZHyabFRwTZa//g5TStMifHc4FFS80kuyGC9CfcdBrowLWIuSdJynbgE0CLBJlUwSKH1r59T+MqOP1JJzCdRT1BceE1SlRCQ1pJONSmnlUaLKAKl/mm0vqJY/Tb6fkjn7jo1nyRNNEbhl3Z7iUc9SKgVSKstU43XmPCST4t0XMMJmN0mkw7Q+vdXNKLvr6memTJUOCRVvugOh4Ple6+uNWDmjzJtIC0y88EwmSt8WmHtx6jVLy/rbq8ptP7zWwr/pS7cRD0zhSLqQp3UMojGE2dMVWxgWoNIC2MpDp+WTZU56swSAWj9l0crKowxBjgMhFnIdI4ZOWMerUakC7ErfFrsitxC63c0/gNODmOAvcFDtbwR1THt1GBaJIBip2ALLVutJzvm0PqYRytcomwl1ENbTs6bb/BJwEhx4NAKq4m1WAU9hZYbJCf7rRv6TfJO+5OPI3wypS+1+abc7spMDb6uGo+uFjZwAV2nbl3qfl3tMUYr1IV4Af3RX69GRYBwfSNTEnN7lY2rHhR033/9CQV25oi3hMK/++GN7O+YGN6OyU236UnUxhsidObcaAaINzACIgwPuIjGic2ETreiyz7cXpc9Xo5+TdP6miFwj0MceOK8EXfah+9v4cWl8/I2PuxGx0UjvjIYtL+mwWjKlqD2OJAU4Wr7rYegWaFY1ekZsJM+KEMk/Xam6DG0aAYkhucVBI9CEzu4i+23HoRu/SIC7XbDWHx/5d8BOEqo8aqNx+KKttO0syCWipNXHQfstWLhS4agY046Q+jglaVzahbHgaROuVgjIRbf0ZIh2zm5CIsEusb2Ww9Dk86qqMkj2UcuwrLzFhYbFFPVpAvZD3nkXNzKIIinDH37rQeCzlLpIiSOeJq5mMFpsE/Zy8lnRdFsYDHAqPk1YkTrYD6OtwDYzIGXvAKZk4M9GZKbuT012EJHLi6qALCHruLZ96sFU7hNZN+vFU1GWHiL+vWDWVk5xbSnB1NkT5TrXivu2RrmW3FaV0wlkV9eel1oMkFuftn3aXHPsnorwrphi/Ty2xBWlz0mXAeK88J1J0QpX0tIoseqIOizRrjU7eN9LulkxojvT6nL5D2sSSHHgMuMdnzXzQCULH6aXS57jpnYS5SnwF2JndRaufOWXny8nE2LXvxVEfWixAtsGbMW/B2Rq/iGZr20PrtxkXyVh5c92kZiA1oHbEsJ0NMTIzX4pqCT3NAvoyLao4QCVlTIERPE3ZuniDpkX4pE2wB2/9XUAGIAvGEmj0oVcyzB19i2pMRL4F0sVS1PatapQAdr9K0DXEJvBOplKXsMkraikmIEKYC8jAveeYhTonEJvte3Cyt3RpJ2w7McxcdSQ1BU9V2rF4BX8AVWbKWKIypV3XlYHcCEegKLH+s5JeBVVTFu9jDT9hX4KrFRL2qFdVRQVJV9lwasiPViTKK9hEWlGnt7nkaFo8qn92HNGnwI9LA4oQu5c1dgo9STYpnj+j1kmT0LeGBU14ZnSZVPWQ8YAudOPRiXB8fgUKjsN1k5lenoXMCm4kir3ITnytXEU4S/PFNRUY81yfZ70NSfRBN7HFNRlzNIbqc85c45TOxcwhPqiioTY3x/xTNFOWri/UiG9a+eXR1iytEGNbdwagicf89SVAF4AnPDqTxqU2dwKJBDlHOmcpZv7TLrJda55OhfHsvZHnGMrWvssyxOXfP6ySuDaHOiaNfYd7OyHXaXY3fdCPAmr9LekKcdru3Igpg94FkKN1jPscB8zfMlFcM4XkN6BkcdPKueb1TTAxNnn1jtuLiDT+oUBSK7y1vSFUM9nNi0ziV1ooyhcVnjEqsfNoJpndvkCfO7Js8mVuqGvL/xmKpcUpXs3WIKbG5w462xwV6WeFrhkzIOzYAPhetguINR9audA7eekdxUC9spYvu68T6FmKzuMs32kG/9Tl80IWhf8NWnbqjbFpk9rKWQkk+5qGKYcm2HC0PvpoysPdJTnq1Vij1HnGI7PF2sL5vJErnd6V0ZnBQOP1f4WZeU4NQfoSHLl6NBb3rmvcc77Q27NzW5xjcT3iO14jdnXFwv0yRW8X/fwHtbuea9tKxyXmePSOmDsuzp8nOkfaHqy2LMH4w0a78PqUwz0izQuTmWWAlJeehcHUNMlUdlPZd/uMRcSZWVlIf2Vaq5f32SIujsTUyVl2VcU3E0rrb4MZaUPiiTSU9DY5keR4So14zSON9dYPdudG4xh8jJ0N+X9RgmH/fTkSEbcKzk/aaMOuq9Fu2LozFd3si6G9qGP0eNf8lm2Su76duK+0Z7Ohx2l8vlYDjsNU/0nxr+Bxt3STlGclSXAAAAAElFTkSuQmCC"
                    />
                  </span>
                  <span className="btn-inner--text ">Госуслуги</span>
                </Button>
              </div>
            </CardHeader>
            <CardBody className="px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <small>Или введите данные вручную</small>
              </div>
              <Form role="form">
                <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-single-02" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Фамилия и имя" type="text" />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="E-mail" type="email" autoComplete="new-email" />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Пароль" type="password" autoComplete="new-password" />
                  </InputGroup>
                </FormGroup>
                <Row className="my-4">
                  <Col xs="12">
                    <div className="custom-control custom-control-alternative custom-checkbox">
                      <input className="custom-control-input" id="customCheckRegister" type="checkbox" />
                      <label className="custom-control-label" htmlFor="customCheckRegister">
                        <span className="text-muted">
                          Я согласен с{' '}
                          <a href="#" onClick={(e) => e.preventDefault()}>
                            Условиями использования
                          </a>
                        </span>
                      </label>
                    </div>
                  </Col>
                </Row>
                <div className="text-center">
                  <Button className="mt-4" color="primary" type="button">
                    Создать аккаунт
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
          <Row className="mt-3">
            <Col className="text-right" lg="12">
              <Link className="text-light" to="/auth/login">
                <small>У меня уже есть аккаунт</small>
              </Link>
            </Col>
          </Row>
        </Col>
      </>
    );
  }
}

export default Register;
