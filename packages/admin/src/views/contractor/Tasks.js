import React, { useEffect, useState } from 'react';
import { Card, CardHeader, Button, Container, Row, Table, Badge } from 'reactstrap';
import HeaderWrapper from 'components/Headers/HeaderWrapper.js';

const Tasks = () => {
  return (
    <>
      <HeaderWrapper gradientColor="info" />
      <Container className=" mt--7" fluid>
        <Row>
          <div className=" col">
            <Card className=" shadow">
              <CardHeader className=" bg-transparent">
                <h3 className=" mb-0">Задачи</h3>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Статус</th>
                    <th scope="col">Название</th>
                    <th scope="col">Описание</th>
                    <th scope="col" />
                  </tr>
                </thead>
                <tbody>
                  {[{ id: '1', name: 'Тестовая задача', description: 'Описание задачи' }].map((task) => (
                    <tr key={task.id}>
                      <td>
                        <Badge color="info">К выполнению</Badge>
                      </td>
                      <th className="max-w-200-px text-overflow">{task.name}</th>
                      <td>{task.description}</td>
                      <td className="d-flex align-items-center justify-content-end flex-nowrap">
                        {!task.approved && (
                          <Button type="button" className="btn-sm btn-success" onClick={() => {}}>
                            Завершить
                          </Button>
                        )}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Tasks;
