import React from 'react';
// react component that copies the given text inside your clipboard
import { CopyToClipboard } from 'react-copy-to-clipboard';
// reactstrap components
import { Card, CardHeader, CardBody, Container, Row, Col, UncontrolledTooltip } from 'reactstrap';
// core components
import HeaderWrapper from 'components/Headers/HeaderWrapper.js';

class Settings extends React.Component {
  state = {};
  render() {
    return (
      <>
        <HeaderWrapper gradientColor="gray-dark" />
        {/* Page content */}
        <Container className=" mt--7" fluid>
          {/* Table */}
          <Row>
            <div className=" col">
              <Card className=" shadow">
                <CardHeader className=" bg-transparent">
                  <h3 className=" mb-0">Настройки</h3>
                </CardHeader>
                <CardBody>Страница находится в разработке</CardBody>
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default Settings;
