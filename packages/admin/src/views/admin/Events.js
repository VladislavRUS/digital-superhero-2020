import React, { useState, useEffect, useMemo } from 'react';

import {
  Badge,
  Card,
  CardHeader,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Progress,
  Table,
  Container,
  Row,
  UncontrolledTooltip,
  Button,
  Input,
  Col,
} from 'reactstrap';
import DatePicker from 'react-datepicker';
import HeaderWrapper from 'components/Headers/HeaderWrapper.js';
import Alerter from 'common/Alerter';
import { getEvents, createEvent, editEvent, deleteEvent } from 'api';
import format from 'date-fns/format';
import isBefore from 'date-fns/isBefore';
import isAfter from 'date-fns/isAfter';
import { formatDate } from 'helpers/dates';

const EventStatus = ({ event }) => {
  if (event.approved) {
    return <Badge color="success">Подтверждено</Badge>;
  }
  return <Badge color="info">На рассмотрении</Badge>;
};

const Events = () => {
  const [events, setEvents] = useState([]);
  const [currentEvent, setCurrentEvent] = useState(null);
  const [showEventModal, setShowEventModal] = useState(false);
  const [filterStatus, setFilterStatus] = useState(null);

  const filteredEvents = useMemo(() => {
    switch (filterStatus) {
      case 'approve':
        return events.filter(({ approved }) => approved);
      case 'waiting':
        return events.filter(({ approved }) => !approved);
      case 'reject':
        return [];
      default:
        return events;
    }
  }, [events, filterStatus]);

  useEffect(() => {
    fetchEvents();
  }, []);

  const fetchEvents = async () => {
    try {
      const { data } = await getEvents();
      console.log(data);
      setEvents(data);
      return data;
    } catch (e) {
      Alerter.error('Error');
    }
  };

  const showNewEventModal = () => {
    setCurrentEvent(null);
    setShowEventModal(true);
  };

  const showEditEventModal = (event) => {
    setCurrentEvent(event);
    setShowEventModal(true);
  };

  const createNewEvent = async (values) => {
    try {
      const { data } = await createEvent(values);
      Alerter.success(
        `Организация "${data.name}" успешно создана! На почту ${data.email} были отправлены данные для входа в систему`,
      );
      setShowEventModal(false);
      setEvents([...events, data]);
    } catch (e) {
      Alerter.error('Произошла ошибка при создании организации');
    }
  };

  const editCurrentEvent = async (values) => {
    try {
      const { data } = await editEvent(values);
      Alerter.success(`Организация "${data.name}" успешно обновлена`);
      setShowEventModal(false);
      const newEvents = events.map((item) => (item.id === data.id ? data : item));
      setEvents(newEvents);
      setCurrentEvent(null);
    } catch (e) {
      Alerter.error('Произошла ошибка при редактировании организации');
    }
  };

  const deleteCurrentEvent = async (event) => {
    try {
      await deleteEvent(event.id);
      Alerter.success(`Организация "${event.name}" успешно удалена`);
      const newEvents = events.filter((item) => item.id !== event.id);
      setEvents(newEvents);
    } catch (e) {
      Alerter.error('Произошла ошибка при удалении организации');
    }
  };

  const approveEvent = async (event) => {
    try {
      const { data } = await editEvent({ id: event.id, approved: true });
      Alerter.success(`Мероприятие "${data.name}" было подтверждено`);
      const newEvents = events.map((item) => (item.id === data.id ? data : item));
      setEvents(newEvents);
    } catch (e) {
      Alerter.error('Произошла ошибка при подтверждении мероприятия');
    }
  };

  return (
    <>
      <HeaderWrapper>
        <Card className="p-3 d-flex flex-start">
          <Row>
            <Col lg={3} md={6}>
              <div className="form-control-label mb-2">Статус </div>
              <Input type="select" name="select" onChange={(e) => setFilterStatus(e.target.value)}>
                <option value="any">Любой</option>
                <option value="approve">Подтверждено</option>
                <option value="waiting">На рассмотрении</option>
              </Input>
            </Col>
            <Col lg={3} md={6}>
              <div className="form-control-label mb-2">Даты проведения </div>
              <DatePicker selected={new Date()} selectsRange customInput={<Input />} />
            </Col>
          </Row>
        </Card>
      </HeaderWrapper>
      {/* Page content */}
      <Container className="mt--7" fluid>
        {/* Table */}
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <h3 className="mb-0">Мероприятия</h3>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Название</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Даты проведения</th>
                    <th scope="col">Адрес</th>
                    <th scope="col">Участники</th>
                    <th scope="col">Описание</th>
                    <th scope="col" />
                  </tr>
                </thead>
                <tbody>
                  {filteredEvents.map((event) => (
                    <tr key={event.id}>
                      <th scope="row">
                        <Media className="align-items-center d-flex">
                          <a
                            className="avatar rounded-circle mr-3 flex-shrink-0"
                            href="#"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img alt="..." src={event.image || `http://lorempixel.com/78/78/sports?id=${event.id}`} />
                          </a>
                          <Media>
                            <span className="mb-0 text-sm text-wrap">{event.name}</span>
                          </Media>
                        </Media>
                      </th>
                      <td>
                        <EventStatus event={event} />
                      </td>
                      <td>
                        {event.startDate && event.endDate
                          ? `${formatDate(event.startDate)} - ${formatDate(event.startDate)} `
                          : '-'}
                      </td>
                      <td>{event.address}</td>

                      <td>
                        <div className="avatar-group">
                          <a
                            className="avatar avatar-sm"
                            href="#"
                            id="tooltip742438047"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              className="rounded-circle"
                              src={require('assets/img/theme/team-1-800x800.jpg')}
                            />
                          </a>
                          <UncontrolledTooltip delay={0} target="tooltip742438047">
                            Ryan Tompson
                          </UncontrolledTooltip>
                          <a
                            className="avatar avatar-sm"
                            href="#"
                            id="tooltip941738690"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              className="rounded-circle"
                              src={require('assets/img/theme/team-2-800x800.jpg')}
                            />
                          </a>
                          <UncontrolledTooltip delay={0} target="tooltip941738690">
                            Romina Hadid
                          </UncontrolledTooltip>
                          <a
                            className="avatar avatar-sm"
                            href="#"
                            id="tooltip804044742"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              className="rounded-circle"
                              src={require('assets/img/theme/team-3-800x800.jpg')}
                            />
                          </a>
                          <UncontrolledTooltip delay={0} target="tooltip804044742">
                            Alexander Smith
                          </UncontrolledTooltip>
                          <a
                            className="avatar avatar-sm"
                            href="#"
                            id="tooltip996637554"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              className="rounded-circle"
                              src={require('assets/img/theme/team-4-800x800.jpg')}
                            />
                          </a>
                          <UncontrolledTooltip delay={0} target="tooltip996637554">
                            Jessica Doe
                          </UncontrolledTooltip>
                        </div>
                      </td>
                      <td>
                        <span className="text-wrap">{event.description}</span>
                      </td>
                      <td className="text-right">
                        <UncontrolledDropdown>
                          <DropdownToggle
                            className="btn-icon-only text-light"
                            href="#"
                            role="button"
                            size="sm"
                            color=""
                            onClick={(e) => e.preventDefault()}
                          >
                            <i className="fas fa-ellipsis-v" />
                          </DropdownToggle>
                          <DropdownMenu className="dropdown-menu-arrow" right>
                            {!event.approved && (
                              <DropdownItem href="#" onClick={() => approveEvent(event)}>
                                Подвердить
                              </DropdownItem>
                            )}
                            <DropdownItem href="#" onClick={(e) => e.preventDefault()}>
                              Редактировать
                            </DropdownItem>
                            <DropdownItem href="#" onClick={(e) => e.preventDefault()}>
                              Удалить
                            </DropdownItem>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Events;
