import React, { useEffect, useState } from 'react';

import {
  Card,
  CardHeader,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Button,
  Table,
  Container,
  Row,
} from 'reactstrap';
import HeaderWrapper from 'components/Headers/HeaderWrapper.js';
import { getOrganizations, createOrganization, editOrganization, deleteOrganization } from 'api';
import { OrganizationModal } from 'components/Modals/OrganizationModal';
import { ImportOrganizationModal } from 'components/Modals/ImportOrganizationModal';
import Alerter from 'common/Alerter';

const getLogoUrl = ({ name }) => `https://dummyimage.com/48x48/adb5bd/fff.jpg?text=${name[0].toUpperCase()}`;

const Organizations = () => {
  const [organizations, setOrganizations] = useState([]);
  const [currentOrganization, setCurrentOrganization] = useState(null);
  const [showOrganizationModal, setShowOrganizationModal] = useState(false);
  const [showImportOrganizationModal, setShowImportOrganizationModal] = useState(false);

  useEffect(() => {
    fetchOrganizations();
  }, []);

  const fetchOrganizations = async () => {
    try {
      const { data } = await getOrganizations();
      const filteredOrganizations = data.filter(({ isContractor }) => !isContractor);
      setOrganizations(filteredOrganizations);
    } catch (e) {
      Alerter.error('Error');
    }
  };

  const showNewOrganizationModal = () => {
    setCurrentOrganization(null);
    setShowOrganizationModal(true);
  };

  const showEditOrganizationModal = (organization) => {
    setCurrentOrganization(organization);
    setShowOrganizationModal(true);
  };

  const createNewOrganization = async (values) => {
    try {
      const { data } = await createOrganization({
        ...values,
        logo: getLogoUrl(values),
      });
      Alerter.success(
        `Организация "${data.name}" успешно создана! На почту ${data.email} были отправлены данные для входа в систему`,
      );
      setShowOrganizationModal(false);
      setOrganizations([...organizations, data]);
    } catch (e) {
      Alerter.error('Произошла ошибка при создании организации');
    }
  };

  const addOrganizations = async (items) => {
    setShowImportOrganizationModal(false);
    try {
      const promises = items.map((item) =>
        createOrganization({ ...item, logo: getLogoUrl(item) }).then(({ data }) =>
          setOrganizations((prev) => [...prev, data]),
        ),
      );
      await Promise.all(promises);
      Alerter.success(`Организации были успешно добавлены`);
    } catch (_) {
      Alerter.error('Произошла ошибка при добавлении организаций');
    }
  };

  const editCurrentOrganization = async (values) => {
    try {
      const { data } = await editOrganization(values);
      Alerter.success(`Организация "${data.name}" успешно обновлена`);
      setShowOrganizationModal(false);
      const newOranizations = organizations.map((item) => (item.id === data.id ? data : item));
      setOrganizations(newOranizations);
      setCurrentOrganization(null);
    } catch (e) {
      Alerter.error('Произошла ошибка при редактировании организации');
    }
  };

  const deleteCurrentOrganization = async (organization) => {
    try {
      await deleteOrganization(organization.id);
      Alerter.success(`Организация "${organization.name}" успешно удалена`);
      const newOranizations = organizations.filter((item) => item.id !== organization.id);
      setOrganizations(newOranizations);
    } catch (e) {
      Alerter.error('Произошла ошибка при удалении организации');
    }
  };

  return (
    <>
      <HeaderWrapper gradientColor="indigo">
        <div className="text-right">
          <Button onClick={() => setShowImportOrganizationModal(true)}>Импортировать организации</Button>
          <Button onClick={showNewOrganizationModal}>Создать новую организацию</Button>
        </div>
      </HeaderWrapper>
      <Container className="mt--7" fluid>
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <h3 className="mb-0">Организации {organizations.length > 0 && `(${organizations.length})`}</h3>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Название</th>
                    <th scope="col">Адрес</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Сайт</th>
                    <th scope="col">Описание</th>
                    <th scope="col" />
                  </tr>
                </thead>
                <tbody>
                  {organizations.map((organization) => (
                    <tr key={organization.id}>
                      <th scope="row">
                        <Media className="align-items-center">
                          <a
                            className="avatar bg-white rounded-circle mr-3 flex-shrink-0"
                            href="#"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img alt="..." src={organization.logo} />
                          </a>
                          <Media>
                            <span className="mb-0 text-sm text-wrap">{organization.name}</span>
                          </Media>
                        </Media>
                      </th>
                      <td>
                        <span className="max-w-200-px text-wrap">{organization.address}</span>
                      </td>
                      <td>{organization.phone}</td>
                      <td>{organization.email}</td>
                      <td>{organization.site}</td>
                      <td>
                        <span className="max-w-200-px text-wrap">{organization.description}</span>
                      </td>

                      <td className="text-right">
                        <UncontrolledDropdown modifiers={{ preventOverflow: { boundariesElement: 'window' } }}>
                          <DropdownToggle
                            className="btn-icon-only text-light"
                            href="#"
                            role="button"
                            size="sm"
                            color=""
                            onClick={(e) => e.preventDefault()}
                          >
                            <i className="fas fa-ellipsis-v" />
                          </DropdownToggle>
                          <DropdownMenu className="dropdown-menu-arrow" right>
                            <DropdownItem onClick={() => showEditOrganizationModal(organization)}>
                              Редактировать
                            </DropdownItem>
                            <DropdownItem onClick={() => deleteCurrentOrganization(organization)}>Удалить</DropdownItem>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card>
          </div>
        </Row>
      </Container>
      <OrganizationModal
        isOpen={showOrganizationModal}
        toggle={() => setShowOrganizationModal(!showOrganizationModal)}
        organization={currentOrganization}
        onSubmit={(values) => (currentOrganization ? editCurrentOrganization(values) : createNewOrganization(values))}
      />
      <ImportOrganizationModal
        isOpen={showImportOrganizationModal}
        toggle={() => setShowImportOrganizationModal(!showImportOrganizationModal)}
        onSubmit={addOrganizations}
      />
    </>
  );
};

export default Organizations;
