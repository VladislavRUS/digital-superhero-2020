/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react';
// react component that copies the given text inside your clipboard
import { CopyToClipboard } from 'react-copy-to-clipboard';
// reactstrap components
import { Card, CardHeader, CardBody, Container, Row, Col, UncontrolledTooltip } from 'reactstrap';
// core components
import HeaderWrapper from 'components/Headers/HeaderWrapper.js';

class Settings extends React.Component {
  state = {};
  render() {
    return (
      <>
        <HeaderWrapper gradientColor="gray-dark" />
        {/* Page content */}
        <Container className=" mt--7" fluid>
          {/* Table */}
          <Row>
            <div className=" col">
              <Card className=" shadow">
                <CardHeader className=" bg-transparent">
                  <h3 className=" mb-0">Настройки</h3>
                </CardHeader>
                <CardBody>Страница находится в разработке</CardBody>
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default Settings;
