import React from 'react';
import { Card, CardHeader, CardBody, Container, Row, Col, UncontrolledTooltip } from 'reactstrap';
import HeaderWrapper from 'components/Headers/HeaderWrapper.js';
import { ParticipantsTable } from 'components/Tables/ParticipantsTable.js';

class Participants extends React.Component {
  state = {};
  render() {
    return (
      <>
        <HeaderWrapper gradientColor="warning" />
        {/* Page content */}
        <Container className=" mt--7" fluid>
          {/* Table */}
          <Row>
            <div className=" col">
              <Card className=" shadow">
                <CardHeader className=" bg-transparent">
                  <h3 className=" mb-0">Участники</h3>
                </CardHeader>
                <ParticipantsTable />
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default Participants;
