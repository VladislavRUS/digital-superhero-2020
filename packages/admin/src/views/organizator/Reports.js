import React, { useContext, useEffect, useState } from 'react';
import { Card, CardHeader, Button, Container, Row, Table, Badge, UncontrolledTooltip } from 'reactstrap';
import HeaderWrapper from 'components/Headers/HeaderWrapper.js';
import { getAllReports, approveReport, getOrganizationEvents, deleteReport } from 'api';
import Alerter from 'common/Alerter';
import { getOrganizations } from 'api';
import { getFilePath } from 'api';
import { UserContext } from 'App';

const Reports = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [reports, setReports] = useState([]);
  const [events, setEvents] = useState([]);

  const { user } = useContext(UserContext);

  useEffect(() => {
    fetchReports();
  }, []);

  const fetchReports = async () => {
    setIsLoading(true);
    try {
      const { data: reports } = await getAllReports();
      const { data: events } = await getOrganizationEvents(user.id);
      const { data: organizations } = await getOrganizations();

      const data = reports
        .map((report) => {
          const event = events.find((e) => e.id === report.eventId);
          const organization = event && organizations.find((o) => o.id === event.organizationId);
          return { ...report, event, organization };
        })
        .filter(({ event }) => !!event);
      console.log(data, events);
      setReports(data);
    } catch (_) {
      console.log(_);
      Alerter.error('Произошла ошибка при загрузке отчетов');
    } finally {
      setIsLoading(false);
    }
  };

  const onDeleteReport = async (id) => {
    try {
      await deleteReport(id);
      const newReports = reports.filter((report) => report.id !== id);
      setReports(newReports);
      Alerter.success('Отчет был успешно удален');
    } catch (_) {
      Alerter.error('Произошла ошибка при удалении отчета');
    }
  };

  return (
    <>
      <HeaderWrapper gradientColor="info" />
      <Container className=" mt--7" fluid>
        <Row>
          <div className=" col">
            <Card className=" shadow">
              <CardHeader className=" bg-transparent">
                <h3 className=" mb-0">Отчеты</h3>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Статус</th>
                    <th scope="col">Название</th>
                    <th scope="col">Мероприятие</th>
                    <th scope="col">Организатор</th>
                    <th scope="col" />
                  </tr>
                </thead>
                <tbody>
                  {reports.map((report) => (
                    <tr key={report.id}>
                      <td>
                        {report.approved && <Badge color="success">Подтверждено</Badge>}
                        {!report.approved && <Badge color="info">На рассмотрении</Badge>}
                      </td>
                      <td className="max-w-200-px text-overflow">
                        <a
                          href={getFilePath(report.path)}
                          className="text-info text-underline"
                          target="_blank"
                          download
                        >
                          {report.name}
                        </a>
                      </td>
                      <td>{report.event && report.event.name}</td>
                      <td>{report.organization && report.organization.name}</td>
                      <td className="d-flex align-items-center justify-content-end flex-nowrap">
                        <Button
                          type="button"
                          className="btn-icon-only btn-white text-danger ml-1"
                          onClick={() => onDeleteReport(report.id)}
                        >
                          <i className="fas fa-trash-alt" />
                        </Button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Reports;
