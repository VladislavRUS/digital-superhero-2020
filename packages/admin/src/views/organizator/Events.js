import React, { useState, useEffect, useContext } from 'react';

import {
  Badge,
  Card,
  CardHeader,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Progress,
  Table,
  Container,
  Row,
  UncontrolledTooltip,
  Button,
} from 'reactstrap';
import HeaderWrapper from 'components/Headers/HeaderWrapper.js';
import Alerter from 'common/Alerter';
import { getOrganizationEvents, createEvent, editEvent, deleteEvent } from 'api';
import format from 'date-fns/format';
import isBefore from 'date-fns/isBefore';
import isAfter from 'date-fns/isAfter';
import { UserContext } from 'App';
import { CreateEventModal } from 'components/Modals/CreateEventModal';
import { EditEventModal } from 'components/Modals/EditEventModal';
import { formatDate } from 'helpers/dates';

// const EventStatus = ({ event }) => {
//   if (isBefore(new Date(), new Date(event.endDate))) {
//     return (
//       <Badge color="" className="badge-dot mr-4">
//         <i className="bg-yellow" />
//         Запланировано
//       </Badge>
//     );
//   }
//   if (isAfter(new Date(), new Date(event.startDate))) {
//     return (
//       <Badge color="" className="badge-dot mr-4">
//         <i className="bg-success" />
//         Завершено
//       </Badge>
//     );
//   }
//   return (
//     <Badge color="" className="badge-dot mr-4">
//       <i className="bg-info" />
//       Идет сейчас
//     </Badge>
//   );
// };

const EventStatus = ({ event }) => {
  if (event.approved) {
    return <Badge color="success">Подтверждено</Badge>;
  }
  return <Badge color="info">На рассмотрении</Badge>;
};

const Events = () => {
  const [events, setEvents] = useState([]);
  const [currentEvent, setCurrentEvent] = useState(null);
  const [showEventModal, setShowEventModal] = useState(false);

  const { user } = useContext(UserContext);

  useEffect(() => {
    fetchEvents();
  }, []);

  const fetchEvents = async () => {
    try {
      const { data } = await getOrganizationEvents(user.id);
      console.log(data);
      setEvents(data);
      return data;
    } catch (e) {
      Alerter.error('Error');
    }
  };

  const showNewEventModal = () => {
    setCurrentEvent(null);
    setShowEventModal(true);
  };

  const showEditEventModal = (event) => {
    setCurrentEvent(event);
    setShowEventModal(true);
  };

  const createNewEvent = async (values) => {
    try {
      const { data } = await createEvent({ ...values, organizationId: user.id });
      Alerter.success(`Мероприятие "${data.name}" успешно создано`);
      setShowEventModal(false);
      setEvents([...events, data]);
    } catch (e) {
      Alerter.error('Произошла ошибка при создании мероприятия');
    }
  };

  const editCurrentEvent = async (values) => {
    try {
      const { data } = await editEvent({ ...values, organizationId: user.id });
      Alerter.success(`Мероприятие "${data.name}" успешно обновлено`);
      setShowEventModal(false);
      const newOranizations = events.map((item) => (item.id === data.id ? data : item));
      setEvents(newOranizations);
      setCurrentEvent(null);
    } catch (e) {
      Alerter.error('Произошла ошибка при редактировании мероприятия');
    }
  };

  const deleteCurrentEvent = async (event) => {
    try {
      await deleteEvent(event.id);
      Alerter.success(`Мероприятие "${event.name}" успешно удалена`);
      const newOranizations = events.filter((item) => item.id !== event.id);
      setEvents(newOranizations);
    } catch (e) {
      Alerter.error('Произошла ошибка при удалении мероприятия');
    }
  };

  return (
    <>
      <HeaderWrapper>
        <div className="d-flex justify-content-end">
          <Button onClick={showNewEventModal}>Создать мероприятие</Button>
        </div>
      </HeaderWrapper>
      <Container className="mt--7" fluid>
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <h3 className="mb-0">Мероприятия</h3>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive hover>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Название</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Даты проведения</th>
                    <th scope="col">Адрес</th>
                    <th scope="col">Участники</th>
                    <th scope="col">Выполнено задач</th>
                    <th scope="col">Описание</th>
                  </tr>
                </thead>
                <tbody>
                  {events.map((event) => (
                    <tr key={event.id} onClick={() => showEditEventModal(event)}>
                      <th scope="row">
                        <Media className="align-items-center d-flex">
                          <a
                            className="avatar rounded-circle mr-3 flex-shrink-0"
                            href="#"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img alt="..." src={event.image || `http://lorempixel.com/78/78/sports?id=${event.id}`} />
                          </a>
                          <Media>
                            <span className="mb-0 text-sm text-wrap">{event.name}</span>
                          </Media>
                        </Media>
                      </th>
                      <td>
                        <EventStatus event={event} />
                      </td>
                      <td>
                        {event.startDate && event.endDate
                          ? `${formatDate(event.startDate)} - ${formatDate(event.endDate)} `
                          : '-'}
                      </td>
                      <td>{event.address}</td>
                      <td>
                        <div className="avatar-group">
                          <a
                            className="avatar avatar-sm"
                            href="#"
                            id="tooltip742438047"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              className="rounded-circle"
                              src={require('assets/img/theme/team-1-800x800.jpg')}
                            />
                          </a>

                          <a
                            className="avatar avatar-sm"
                            href="#"
                            id="tooltip941738690"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              className="rounded-circle"
                              src={require('assets/img/theme/team-2-800x800.jpg')}
                            />
                          </a>
                          <a
                            className="avatar avatar-sm"
                            href="#"
                            id="tooltip804044742"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              className="rounded-circle"
                              src={require('assets/img/theme/team-3-800x800.jpg')}
                            />
                          </a>
                          <a
                            className="avatar avatar-sm"
                            href="#"
                            id="tooltip996637554"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              className="rounded-circle"
                              src={require('assets/img/theme/team-4-800x800.jpg')}
                            />
                          </a>
                        </div>
                      </td>
                      <td>
                        <div className="d-flex align-items-center">
                          <span className="mr-2">60%</span>
                          <div>
                            <Progress max="100" value="60" barClassName="bg-danger" />
                          </div>
                        </div>
                      </td>
                      <td>
                        <span className="text-wrap">{event.description}</span>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card>
          </div>
        </Row>
      </Container>
      <CreateEventModal
        isOpen={showEventModal && !currentEvent}
        toggle={() => setShowEventModal(!showEventModal)}
        onSubmit={createNewEvent}
      />
      {currentEvent && (
        <EditEventModal
          isOpen={showEventModal && !!currentEvent}
          toggle={() => setShowEventModal(!showEventModal)}
          event={currentEvent}
          onSubmit={editCurrentEvent}
        />
      )}
    </>
  );
};

export default Events;
