import React, { useState, useEffect } from 'react';
import {
  Card,
  CardHeader,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Button,
  Table,
  Container,
  Row,
} from 'reactstrap';
import HeaderWrapper from 'components/Headers/HeaderWrapper.js';
import Alerter from 'common/Alerter';
import { createOrganization, editOrganization, deleteOrganization, getOrganizations } from 'api';
import { ContractorModal } from 'components/Modals/ContractorModal';

const Contractors = () => {
  const [contractors, setContractors] = useState([]);
  const [currentContractor, setCurrentContractor] = useState(null);
  const [showContractorModal, setShowContractorModal] = useState(false);

  useEffect(() => {
    fetchContractors();
  }, []);

  const fetchContractors = async () => {
    try {
      const { data } = await getOrganizations();
      const filteredContractors = data.filter(({ isContractor }) => isContractor);
      setContractors(filteredContractors);
    } catch (e) {
      Alerter.error('Error');
    }
  };

  const showNewContractorModal = () => {
    setCurrentContractor(null);
    setShowContractorModal(true);
  };

  const showEditContractorModal = (contractor) => {
    setCurrentContractor(contractor);
    setShowContractorModal(true);
  };

  const createNewContractor = async (values) => {
    try {
      const { data } = await createOrganization({
        ...values,
        logo: `https://dummyimage.com/48x48/adb5bd/fff.jpg?text=${values.name[0].toUpperCase()}`,
        isContractor: true,
      });
      Alerter.success(
        `Исполнитель "${data.name}" успешно создан! На почту ${data.email} были отправлены данные для входа в систему`,
      );
      setShowContractorModal(false);
      setContractors([...contractors, data]);
    } catch (e) {
      Alerter.error('Произошла ошибка при создании исполнителя');
    }
  };

  const editCurrentContractor = async (values) => {
    try {
      const { data } = await editOrganization(values);
      Alerter.success(`Исполнитель "${data.name}" успешно обновлен`);
      setShowContractorModal(false);
      const newOranizations = contractors.map((item) => (item.id === data.id ? data : item));
      setContractors(newOranizations);
      setCurrentContractor(null);
    } catch (e) {
      Alerter.error('Произошла ошибка при редактировании исполнителя');
    }
  };

  const deleteCurrentContractor = async (contractor) => {
    try {
      await deleteOrganization(contractor.id);
      Alerter.success(`Исполнитель "${contractor.name}" успешно удалена`);
      const newOranizations = contractors.filter((item) => item.id !== contractor.id);
      setContractors(newOranizations);
    } catch (e) {
      Alerter.error('Произошла ошибка при удалении исполнителя');
    }
  };

  return (
    <>
      <HeaderWrapper gradientColor="indigo">
        <div className="d-flex justify-content-end">
          <Button onClick={showNewContractorModal}>Добавить исполнителя</Button>
        </div>
      </HeaderWrapper>
      <Container className="mt--7" fluid>
        <Row>
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <h3 className="mb-0">Исполнители {contractors.length > 0 && `(${contractors.length})`}</h3>
              </CardHeader>
              <Table className="align-items-center table-flush" responsive>
                <thead className="thead-light">
                  <tr>
                    <th scope="col">Название</th>
                    <th scope="col">Адрес</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Сайт</th>
                    <th scope="col">Описание</th>
                    <th scope="col" />
                  </tr>
                </thead>
                <tbody>
                  {contractors.map((contractor) => (
                    <tr key={contractor.id}>
                      <th scope="row">
                        <Media className="align-items-center">
                          <a
                            className="avatar bg-white rounded-circle mr-3 flex-shrink-0"
                            href="#"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img alt="..." src={contractor.logo} />
                          </a>
                          <Media>
                            <span className="mb-0 text-sm text-wrap">{contractor.name}</span>
                          </Media>
                        </Media>
                      </th>
                      <td>
                        <span className="max-w-200-px text-wrap">{contractor.address}</span>
                      </td>
                      <td>{contractor.phone}</td>
                      <td>{contractor.email}</td>
                      <td>{contractor.site}</td>
                      <td>
                        <span className="max-w-200-px text-wrap">{contractor.description}</span>
                      </td>

                      <td className="text-right">
                        <UncontrolledDropdown modifiers={{ preventOverflow: { boundariesElement: 'window' } }}>
                          <DropdownToggle
                            className="btn-icon-only text-light"
                            href="#"
                            role="button"
                            size="sm"
                            color=""
                            onClick={(e) => e.preventDefault()}
                          >
                            <i className="fas fa-ellipsis-v" />
                          </DropdownToggle>
                          <DropdownMenu className="dropdown-menu-arrow" right>
                            <DropdownItem onClick={() => showEditContractorModal(contractor)}>
                              Редактировать
                            </DropdownItem>
                            <DropdownItem onClick={() => deleteCurrentContractor(contractor)}>Удалить</DropdownItem>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card>
          </div>
        </Row>
      </Container>
      <ContractorModal
        isOpen={showContractorModal}
        toggle={() => setShowContractorModal(!showContractorModal)}
        contractor={currentContractor}
        onSubmit={(values) => (currentContractor ? editCurrentContractor(values) : createNewContractor(values))}
      />
    </>
  );
};

export default Contractors;
