const FileController = require('../controllers/file.controller');

const fileRoutes = (app) => {
  app.post('/upload', FileController.upload);
  app.post('/files', FileController.create);
  app.get('/files', FileController.find);
  app.put('/files/:fileId', FileController.update);
  app.delete('/files/:fileId', FileController.deleteById);
};

module.exports = {
  fileRoutes,
};
