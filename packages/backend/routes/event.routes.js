const EventController = require('../controllers/event.controller');

const eventRoutes = (app) => {
  app.post('/events', EventController.create);
  app.get('/events', EventController.findAll);
  app.put('/events/:eventId', EventController.update);
  app.delete('/events/:eventId', EventController.deleteById);
};

module.exports = {
  eventRoutes,
};
