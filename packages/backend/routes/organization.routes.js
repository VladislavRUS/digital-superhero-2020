const OrganizationController = require('../controllers/organization.controller');

const organizationRoutes = (app) => {
  app.post('/organizations', OrganizationController.create);
  app.get('/organizations', OrganizationController.findAll);
  app.post('/organizations/login', OrganizationController.login);
  app.get('/organizations/:organizationId', OrganizationController.findById);
  app.put('/organizations/:organizationId', OrganizationController.update);
  app.delete('/organizations/:organizationId', OrganizationController.remove);
  app.get('/public-organizations', OrganizationController.getPublicOrganizations);
};

module.exports = {
  organizationRoutes,
};
