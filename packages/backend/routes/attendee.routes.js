const AttendeeController = require('../controllers/attendee.controller');

const attendeeRoutes = (app) => {
  app.get('/attendees', AttendeeController.findAll);
  app.get('/attendees/:attendeeId', AttendeeController.findById);
  app.post('/attendees', AttendeeController.create);
  app.post('/attendees/login', AttendeeController.login);
  app.post('/attendees/register-vk', AttendeeController.registerVk);
  app.post('/attendees/login-vk', AttendeeController.loginVk);
  app.put('/attendees/:attendeeId', AttendeeController.update);
  app.post('/attendees/:attendeeId/participate', AttendeeController.participate);
};

module.exports = {
  attendeeRoutes,
};
