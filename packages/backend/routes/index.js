const { organizationRoutes } = require('./organization.routes');
const { eventRoutes } = require('./event.routes');
const { attendeeRoutes } = require('./attendee.routes');
const { fileRoutes } = require('./file.routes');

const initRoutes = (app) => {
  organizationRoutes(app);
  eventRoutes(app);
  attendeeRoutes(app);
  fileRoutes(app);
};

module.exports = {
  initRoutes,
};
