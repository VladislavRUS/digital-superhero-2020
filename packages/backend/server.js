const seeds = require('./seeds');
const { initRoutes } = require('./routes');
const { db } = require('./models');
const fileUpload = require('express-fileupload');
const PORT = process.env.PORT || 3001;
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());
app.use(express.static('public'));

initRoutes(app);

async function main() {
  try {
    await db.mongoose.connect(db.url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    await seeds();

    console.log('Connected to the database!');

    await app.listen(PORT);

    console.log(`Server is running on port ${PORT}.`);
  } catch (e) {
    console.log(e);
    process.exit();
  }
}

main();
