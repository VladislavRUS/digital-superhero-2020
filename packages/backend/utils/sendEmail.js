const Mailgun = require('mailgun-js');

const apiKey = 'a2a2299133236123e766d189a72eb1b5-d5e69b0b-b0b2fb85';
const from = 'register@sportify.com';
const domain = 'sandboxbae6fe0ff9c246f3b5d31188b1f4360f.mailgun.org';

const sendEmail = (to, subject, html) => {
  return new Promise((resolve, reject) => {
    const mailgun = new Mailgun({ apiKey, domain });

    const data = {
      from,
      to,
      subject,
      html,
    };

    mailgun.messages().send(data, function (err) {
      //If there is an error, render the error page
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

module.exports = sendEmail;
