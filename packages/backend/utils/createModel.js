const { setSchemaId } = require('./setSchemaId');

const createModel = (mongoose, name, ...schemaParams) => {
  const schema = mongoose.Schema(...schemaParams);

  setSchemaId(schema);

  return mongoose.model(name, schema);
};

module.exports = createModel;
