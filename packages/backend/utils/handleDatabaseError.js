const handleDatabaseError = (error, res) => {
  console.log(error);
  res.status(500).send({ message: error.message || 'Error occurred' });
};

module.exports = {
  handleDatabaseError,
};
