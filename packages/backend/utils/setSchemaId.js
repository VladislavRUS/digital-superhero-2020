const setSchemaId = function (schema) {
  schema.method('toJSON', function () {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });
};

module.exports = {
  setSchemaId,
};
