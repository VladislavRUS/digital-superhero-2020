const createModel = require('../utils/createModel');

module.exports = (mongoose) => {
  return createModel(
    mongoose,
    'file',
    {
      name: String,
      path: String,
      eventId: String,
      approved: {
        type: Boolean,
        default: false,
      },
    },
    { timestamps: true },
  );
};
