const createModel = require('../utils/createModel');

module.exports = (mongoose) => {
  return createModel(
    mongoose,
    'event',
    {
      name: String,
      address: String,
      startDate: String,
      endDate: String,
      description: String,
      expectedAttendeesNumber: Number,
      acceptedAttendeesNumber: {
        type: Number,
        default: 0,
      },
      organizationId: String,
      image: String,
      approved: Boolean,
      cost: {
        type: Number,
        default: 0,
      },
      stream: {
        type: String,
        default: '',
      },
    },
    { timestamps: true },
  );
};
