const createModel = require('../utils/createModel');

module.exports = (mongoose) => {
  return createModel(
    mongoose,
    'organization',
    {
      name: String,
      address: String,
      description: String,
      site: String,
      phone: String,
      logo: String,
      email: String,
      hashedPassword: String,
      isContractor: {
        type: Boolean,
        default: false,
      },
    },
    { timestamps: true },
  );
};
