const createModel = require('../utils/createModel');

module.exports = (mongoose) => {
  return createModel(
    mongoose,
    'attendee',
    {
      firstName: String,
      lastName: String,
      description: {
        type: String,
        default: '',
      },
      email: String,
      hashedPassword: String,
      externalId: String,
      pushNotificationToken: {
        type: String,
        default: '',
      },
      activeEventsIds: {
        type: [String],
        default: [],
      },
      location: String,
      visitedEvents: {
        type: Number,
        default: 0,
      },
      bonuses: {
        type: Number,
        default: 0,
      },
      avatar: {
        type: String,
        default:
          'https://lh3.googleusercontent.com/proxy/uleQOkVdlMoWNnsfAUlbvb6gsMB4JVaJj50Ux1iqtK_nrvZ0BKW1Azww17LODTGzAdJo-W_RBwamYqIhpr7jGmUhDMC82b8iVCObnc6VSvFZ',
      },
    },
    { timestamps: true },
  );
};
