const dbConfig = require('../config/db.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.organizations = require('./organization.model')(mongoose);
db.events = require('./event.model')(mongoose);
db.attendees = require('./attendee.model')(mongoose);
db.files = require('./file.model')(mongoose);

module.exports = {
  db,
};
