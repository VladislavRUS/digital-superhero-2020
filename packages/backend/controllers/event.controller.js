const { handleDatabaseError } = require('../utils/handleDatabaseError');
const { db } = require('../models');
const { Expo } = require('expo-server-sdk');

let expo = new Expo();

const Event = db.events;
const Attendee = db.attendees;

const create = async (req, res) => {
  const body = {
    ...req.body,
    image: `https://eu.ui-avatars.com/api/?name=${req.body.name.slice(0, 1)}&size=256&background=FB6340&color=fff`,
  };

  const event = new Event(body);

  try {
    const data = await event.save();
    res.send(data);

    if (process.env.NODE_ENV !== 'production') {
      return;
    }

    const attendees = await Attendee.find({});
    const messages = [];

    attendees.forEach((attendee) => {
      if (attendee.pushNotificationToken) {
        messages.push({
          to: attendee.pushNotificationToken,
          sound: 'default',
          title: 'Появилось новое мероприятие',
          body: event.name,
        });
      }
    });

    let chunks = expo.chunkPushNotifications(messages);
    let tickets = [];

    for (let chunk of chunks) {
      try {
        let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
        console.log(ticketChunk);
        tickets.push(...ticketChunk);
        // NOTE: If a ticket contains an error code in ticket.details.error, you
        // must handle it appropriately. The error codes are listed in the Expo
        // documentation:
        // https://docs.expo.io/push-notifications/sending-notifications/#individual-errors
      } catch (error) {
        console.error(error);
      }
    }

    let receiptIds = [];
    for (let ticket of tickets) {
      // NOTE: Not all tickets have IDs; for example, tickets for notifications
      // that could not be enqueued will have error information and no receipt ID.
      if (ticket.id) {
        receiptIds.push(ticket.id);
      }
    }

    let receiptIdChunks = expo.chunkPushNotificationReceiptIds(receiptIds);

    for (let chunk of receiptIdChunks) {
      try {
        let receipts = await expo.getPushNotificationReceiptsAsync(chunk);
        console.log(receipts);

        // The receipts specify whether Apple or Google successfully received the
        // notification and information about an error, if one occurred.
        for (let receiptId in receipts) {
          let { status, message, details } = receipts[receiptId];
          if (status === 'ok') {
            continue;
          } else if (status === 'error') {
            console.error(`There was an error sending a notification: ${message}`);
            if (details && details.error) {
              // The error codes are listed in the Expo documentation:
              // https://docs.expo.io/push-notifications/sending-notifications/#individual-errors
              // You must handle the errors appropriately.
              console.error(`The error code is ${details.error}`);
            }
          }
        }
      } catch (error) {
        console.error(error);
      }
    }
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const findAll = async (req, res) => {
  try {
    const data = await Event.find(req.query);
    res.send(data);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const update = async (req, res) => {
  const { eventId } = req.params;

  try {
    const data = await Event.findByIdAndUpdate(eventId, req.body, { useFindAndModify: false, new: true });

    res.send(data);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const deleteById = async (req, res) => {
  const { eventId } = req.params;

  try {
    await Event.findByIdAndRemove(eventId);

    res.send(200);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

module.exports = {
  create,
  findAll,
  update,
  deleteById,
};
