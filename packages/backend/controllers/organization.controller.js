const { handleDatabaseError } = require('../utils/handleDatabaseError');
const sha1 = require('sha1');
const { db } = require('../models');
const sendEmail = require('../utils/sendEmail');
const shortid = require('shortid');
const axios = require('axios');
const faker = require('faker');

const Organization = db.organizations;

const create = async (req, res) => {
  const exists = await Organization.findOne(req.body);

  if (exists) {
    res.sendStatus(404);
    return;
  }

  const password = shortid.generate();

  const body = {
    ...req.body,
    hashedPassword: sha1(password),
  };

  const organization = new Organization(body);

  try {
    const data = await organization.save();

    res.send(data);

    if (process.env.NODE_ENV !== 'production') {
      return;
    }

    await sendEmail(
      req.body.email,
      'Регистрация',
      `
      Вы успешно зарегистрировались как организатор мероприятий. 
      Ваш пароль для входа: ${password}
    `,
    );
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const findAll = async (req, res) => {
  try {
    const data = await Organization.find(req.query);
    res.send(data);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const findById = async (req, res) => {
  const { organizationId } = req.params;

  try {
    const data = await Organization.findById(organizationId);

    if (data) {
      res.send(data);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const data = await Organization.findOne({
      email,
      hashedPassword: sha1(password),
    });

    if (data) {
      res.send(data);
    } else {
      res.sendStatus(401);
    }
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const update = async (req, res) => {
  const { organizationId } = req.params;

  try {
    const data = await Organization.findByIdAndUpdate(organizationId, req.body, { useFindAndModify: false, new: true });

    res.send(data);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const remove = async (req, res) => {
  const { organizationId } = req.params;

  try {
    await Organization.findByIdAndRemove(organizationId);
    res.sendStatus(200);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const getPublicOrganizations = async (req, res) => {
  const url = 'https://opendata.tatar.ru/api/v1/resource/187765f2-db33-4ed9-9770-ef104fa7bfdf';

  try {
    const { data } = await axios.get(url);
    const { records } = data;

    const organizations = records.map((record) => ({
      name: record['Наименование '],
      phone: record['Телефон'],
      address: record['Адрес'],
      email: faker.internet.email(),
    }));

    res.send(organizations);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

module.exports = {
  create,
  findAll,
  login,
  update,
  remove,
  findById,
  getPublicOrganizations,
};
