const { handleDatabaseError } = require('../utils/handleDatabaseError');
const { db } = require('../models');
const sha1 = require('sha1');
const { CLIENT_ID, CLIENT_SECRET } = require('../config');
const axios = require('axios');
const { API } = require('vk-io');

const Attendee = db.attendees;
const Event = db.events;

const findAll = async (req, res) => {
  try {
    const data = await Attendee.find(req.query);
    res.send(data);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const findById = async (req, res) => {
  const { attendeeId } = req.params;

  try {
    const data = await Attendee.findById(attendeeId);

    if (data) {
      res.send(data);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const create = async (req, res) => {
  try {
    const exists = await Attendee.findOne(req.body);

    if (exists) {
      res.sendStatus(404);
      return;
    }

    const { password, ...rest } = req.body;

    const body = {
      ...rest,
      hashedPassword: sha1(password),
      avatar: `https://eu.ui-avatars.com/api/?name=${rest.firstName}+${rest.lastName}&size=256&background=FB6340&color=fff`,
    };

    const attendee = new Attendee(body);

    const data = await attendee.save();

    res.send(data);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const data = await Attendee.findOne({
      email,
      hashedPassword: sha1(password),
    });

    if (data) {
      res.send(data);
    } else {
      res.sendStatus(401);
    }
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const registerVk = async (req, res) => {
  const { code } = req.body;

  const url = `https://oauth.vk.com/access_token?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&redirect_uri=https://online.dshkazan.ru&code=${code}`;

  try {
    const { data } = await axios.get(url);
    const { access_token, user_id, email } = data;

    const exists = await Attendee.findOne({
      externalId: user_id,
    });

    if (exists) {
      res.sendStatus(401);
      return;
    }

    const api = new API({
      token: access_token,
    });

    const users = await api.call('users.get?fields=photo_200,country,interests,activities', {
      user_ids: user_id,
    });

    const [user] = users;

    let description = '-';

    const { interests, activities } = user;

    if (interests && activities) {
      description = `${interests}. ${activities}`;
    } else if (interests && !activities) {
      description = interests;
    } else {
      description = activities;
    }

    const attendee = new Attendee({
      firstName: user.first_name,
      lastName: user.last_name,
      email,
      location: user.country.title,
      avatar: user.photo_200,
      description,
      externalId: user_id,
    });

    await attendee.save();

    res.send(attendee);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const loginVk = async (req, res) => {
  const { code } = req.body;

  const url = `https://oauth.vk.com/access_token?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&redirect_uri=https://online.dshkazan.ru&code=${code}`;

  try {
    const { data } = await axios.get(url);
    const { user_id } = data;

    const attendee = await Attendee.findOne({ externalId: user_id });

    if (attendee) {
      res.send(attendee);
    } else {
      res.sendStatus(401);
    }
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const update = async (req, res) => {
  const { attendeeId } = req.params;

  try {
    const data = await Attendee.findByIdAndUpdate(attendeeId, req.body, { useFindAndModify: false, new: true });

    res.send(data);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

const participate = async (req, res) => {
  const { attendeeId } = req.params;
  const { eventId } = req.query;

  try {
    const event = await Event.findById(eventId);
    await Event.findByIdAndUpdate(
      eventId,
      {
        acceptedAttendeesNumber: event.acceptedAttendeesNumber + 1,
      },
      { useFindAndModify: false, new: true },
    );

    const attendee = await Attendee.findById(attendeeId);

    const body = {
      activeEventsIds: [...attendee.activeEventsIds, eventId],
      bonuses: attendee.bonuses + 10,
    };

    const data = await Attendee.findByIdAndUpdate(attendeeId, body, { useFindAndModify: false, new: true });

    res.send(data);
  } catch (e) {
    handleDatabaseError(e, res);
  }
};

module.exports = {
  create,
  login,
  update,
  findAll,
  participate,
  findById,
  registerVk,
  loginVk,
};
