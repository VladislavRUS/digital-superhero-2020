const { handleDatabaseError } = require('../utils/handleDatabaseError');
const { db } = require('../models');
const shortid = require('shortid');
const path = require('path');

const File = db.files;

const create = async (req, res) => {
  try {
    const file = new File(req.body);
    const data = await file.save();
    res.send(data);
  } catch (e) {
    handleDatabaseError(res);
  }
};

const update = async (req, res) => {
  const { fileId } = req.params;

  try {
    const data = await File.findByIdAndUpdate(fileId, req.body, { useFindAndModify: false, new: true });
    res.send(data);
  } catch (e) {
    handleDatabaseError(res);
  }
};

const deleteById = async (req, res) => {
  const { fileId } = req.params;

  try {
    await File.findByIdAndDelete(fileId);
    res.sendStatus(200);
  } catch (e) {
    handleDatabaseError(res);
  }
};

const find = async (req, res) => {
  try {
    const result = await File.find(req.query);
    res.send(result);
  } catch (e) {
    handleDatabaseError(res);
  }
};

const upload = (req, res) => {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let file = req.files.file;
  const { name } = file;

  const lastDotIndex = name.lastIndexOf('.');
  const extension = lastDotIndex !== -1 ? `${name.slice(lastDotIndex)}` : '';

  const filePath = `/static/${shortid.generate()}${extension}`;
  const fileDestination = path.resolve(`./public/${filePath}`);

  // Use the mv() method to place the file somewhere on your server
  file.mv(fileDestination, function (err) {
    if (err) return res.status(500).send(err);
    res.send({ filePath });
  });
};

module.exports = {
  create,
  upload,
  find,
  update,
  deleteById,
};
