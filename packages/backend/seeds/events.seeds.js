const { db } = require('../models');
const { SEED_ORGANIZATIONS } = require('./organizations.seeds');

const Event = db.events;
const Organization = db.organizations;

const SEED_EVENTS = [
  {
    name: 'Забей гол!',
    address: 'Самара, ул. Мичурина, 46',
    startDate: '2020-09-26T19:00:00.000Z',
    endDate: '2020-09-27T20:00:00.000Z',
    description: 'Соревнование проводится среди старших классов региона',
    expectedAttendeesNumber: 100,
    image:
      'https://image.freepik.com/free-photo/an-agroup-of-kids-playing-soccer-football-for-exercise-in-community-rural-area-under-the-sunset_34141-479.jpg',
    cost: 800,
    approved: true,
    stream: 'https://www.youtube.com/embed/GtMqfC42vXU?autoplay=1',
  },
  {
    name: 'Первенство по волейболу',
    address: 'Самара, ул. Олежко, 22',
    startDate: '2020-10-02T19:44:41.000Z',
    endDate: '2020-10-05T19:44:41.000Z',
    description: 'Участие принимают выпускные классы области',
    expectedAttendeesNumber: 500,
    image: 'https://sites.google.com/site/volejbol06/_/rsrc/1370873388691/home/g40_296.jpeg?height=240&width=320',
    cost: 0,
    approved: true,
    stream: 'https://www.youtube.com/embed/8FxlbLADBt8?autoplay=1',
  },
  {
    name: 'Ралли: WRC 2020',
    address: 'Казань, ул. Якименко, 18б',
    startDate: '2020-10-07T16:00:00.000Z',
    endDate: '2020-10-08T17:00:00.000Z',
    description: 'Заезд на ралли по пересеченной местности. Участников ждут зрелищные гонки',
    expectedAttendeesNumber: 300,
    image: 'https://www.avtosport.ru/rally/DSC08683_490.jpg',
    cost: 0,
    approved: true,
    stream: 'https://www.youtube.com/embed/6UVhX5TeC4c?autoplay=1',
  },
  {
    name: 'Формула-1',
    address: 'Казань, ул. Дедова, 13',
    startDate: '2020-10-10T20:30:00.000Z',
    endDate: '2020-10-11T22:00:00.000Z',
    description: 'Высший класс гонок на автомобилях с открытыми колёсами',
    expectedAttendeesNumber: 3000,
    image: 'https://s0.rbk.ru/v6_top_pics/media/img/6/65/755833369493656.jpg',
    cost: 2000,
    approved: true,
    stream: 'https://www.youtube.com/embed/R_eWDfkTV6Y?autoplay=1',
  },
  {
    name: 'Эстафета: последний рубеж',
    address: 'Казань, ул. Хорова, 10а',
    startDate: '2020-10-13T15:00:00.000Z',
    endDate: '2020-10-13T17:00:00.000Z',
    description: 'Спортивная эстафета с препятствиями. Участники поборются за главный приз - 100 тысяч рублей',
    expectedAttendeesNumber: 500,
    image: 'https://cdnimg.rg.ru/img/content/128/27/42/2_d_850.jpg',
    cost: 0,
    approved: true,
    stream: 'https://www.youtube.com/embed/37Iue5Ynxos?autoplay=1',
  },
  {
    name: 'Перетягивание канатов',
    address: 'Казань, ул. Алексеева, 25',
    startDate: '2020-10-16T20:30:00.000Z',
    endDate: '2020-10-16T22:00:00.000Z',
    description: 'Профессиональное перетягивание канатов - участие примут лучшие спортсмены страны',
    expectedAttendeesNumber: 400,
    image: 'https://showt.ru/wp-content/uploads/2014/02/6b8.jpg',
    cost: 0,
    approved: true,
    stream: 'https://www.youtube.com/embed/kTABsVRmRCc?autoplay=1',
  },
];

const eventsSeeds = async () => {
  for (let i = 0; i < SEED_ORGANIZATIONS.length; i++) {
    const organization = SEED_ORGANIZATIONS[i];

    try {
      const existingOrganization = await Organization.findOne({ name: organization.name });

      const { id: organizationId } = existingOrganization;

      const events = [SEED_EVENTS[i * 2], SEED_EVENTS[i * 2 + 1]];

      for (let j = 0; j < events.length; j++) {
        const event = events[j];

        const exists = await Event.findOne({ organizationId, name: event.name });

        if (!exists) {
          await new Event({
            ...event,
            organizationId,
          }).save();
        } else {
          await Event.findByIdAndUpdate(exists.id, event, { useFindAndModify: false });
        }
      }
    } catch (e) {
      console.log(e);
    }
  }
};

module.exports = {
  eventsSeeds,
  SEED_EVENTS,
};
