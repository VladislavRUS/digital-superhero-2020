const sha1 = require('sha1');
const { db } = require('../models');

const Organization = db.organizations;

const SEED_ORGANIZATIONS = [
  {
    name: 'МБОУ Лицей №34',
    address: 'Самара, ул. Ставропольская, 47',
    description: 'Общеобразовательное учерждение',
    site: 'https://school-34.ru',
    phone: '+734566434',
    email: 'school34@edu.ru',
    logo: 'https://www.logopik.com/wp-content/uploads/edd/2018/11/Education-School-Vector-Logo.png',
    hashedPassword: sha1('password'),
  },
  {
    name: 'ДОССААФ №12',
    address: 'Самара, ул. Макарова, 31',
    description: 'Школа подготовки профессиональных водителей',
    site: 'https://dosaaf-12.ru',
    phone: '+7344512323',
    email: 'dosaaf12@edu.ru',
    logo:
      'https://www.graphicsprings.com/filestorage/stencils/c95112060191cb7abcd9e04a7851eeef.png?width=500&height=500',
    hashedPassword: sha1('password'),
  },
  {
    name: 'ДКИТ Юных пионеров',
    address: 'Самара, ул. Васильева, 21а',
    description: 'Муниципальное учерждение развлекательных мероприятий для молодежи',
    site: 'https://dkit-youth.ru',
    phone: '+745577754',
    email: 'dkit-youth@edu.ru',
    logo: 'https://molod86.ru/wp-content/uploads/2019/04/2511-12.jpg',
    hashedPassword: sha1('password'),
  },
];

const SEED_CONTRACTORS = [
  {
    name: 'MF Group',
    address: 'Самара, ул. Аэродромная, 47а',
    description: 'Фото/видео съемка',
    site: 'https://mf-group.ru',
    phone: '+733345138',
    email: 'mg-group@gmail.ru',
    logo: 'https://eventcatalog.ru/upload/fe25502f771eb35ff2e73e77e06d5135.png',
    hashedPassword: sha1('123'),
    isContractor: true,
  },
];

const organizationsSeeds = async () => {
  for (let i = 0; i < SEED_ORGANIZATIONS.length; i++) {
    const organization = SEED_ORGANIZATIONS[i];

    try {
      const exists = await Organization.findOne({ name: organization.name });

      if (!exists) {
        await new Organization(organization).save();
      } else {
        await Organization.findByIdAndUpdate(exists.id, organization, { useFindAndModify: false });
      }
    } catch (e) {
      console.log(e);
    }
  }

  for (let i = 0; i < SEED_CONTRACTORS.length; i++) {
    const organizationContractor = SEED_CONTRACTORS[i];

    try {
      const exists = await Organization.findOne({ name: organizationContractor.name });

      if (!exists) {
        await new Organization(organizationContractor).save();
      } else {
        await Organization.findByIdAndUpdate(exists.id, organizationContractor, { useFindAndModify: false });
      }
    } catch (e) {
      console.log(e);
    }
  }
};

module.exports = {
  organizationsSeeds,
  SEED_ORGANIZATIONS,
};
