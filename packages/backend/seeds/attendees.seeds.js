const { db } = require('../models');
const { SEED_EVENTS } = require('./events.seeds');
const sha1 = require('sha1');

const Event = db.events;
const Attendee = db.attendees;

const SEED_ATTENDEES = [
  {
    firstName: 'Александр',
    lastName: 'Тихонов',
    email: 'Alex@mail.ru',
    hashedPassword: sha1('password'),
    location: 'Казань, Россия',
    visitedEvents: 34,
    bonuses: 156,
    avatar: 'https://randomuser.me/api/portraits/men/86.jpg',
    description: 'Всем привет! Люблю активные виды спорта и с удовольстием их посещаю',
  },
  {
    firstName: 'Виктория',
    lastName: 'Татищева',
    email: 'Vik@mail.ru',
    hashedPassword: sha1('password'),
    location: 'Самара, Россия',
    visitedEvents: 56,
    bonuses: 210,
    avatar: 'https://randomuser.me/api/portraits/women/67.jpg',
    description: 'Привет! Моя жизнь непосредственно связана со спортом - я сертифицированный фитнес инструктор',
  },
];

const attendeesSeeds = async () => {
  for (let i = 0; i < SEED_EVENTS.length; i++) {
    const event = SEED_EVENTS[i];

    try {
      const existingEvent = await Event.findOne({ name: event.name });

      const { id: eventId } = existingEvent;

      const attendee = SEED_ATTENDEES[i % 2 === 0 ? 0 : 1];

      const exists = await Attendee.findOne({ email: attendee.email });
      attendee.activeEventsIds = exists ? [...exists.activeEventsIds, eventId] : [eventId];

      await Event.findByIdAndUpdate(
        eventId,
        {
          acceptedAttendeesNumber: existingEvent.acceptedAttendeesNumber + 1,
        },
        { useFindAndModify: false, new: true },
      );

      if (!exists) {
        await new Attendee({
          ...attendee,
        }).save();
      } else {
        await Attendee.findByIdAndUpdate(exists.id, attendee, { useFindAndModify: false });
      }
    } catch (e) {
      console.log(e);
    }
  }
};

module.exports = {
  attendeesSeeds,
};
