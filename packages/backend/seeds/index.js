const { organizationsSeeds } = require('./organizations.seeds');
const { eventsSeeds } = require('./events.seeds');
const { attendeesSeeds } = require('./attendees.seeds');

const { db } = require('../models');

const seeds = async () => {
  await db.organizations.deleteMany({});
  await db.events.deleteMany({});
  await db.attendees.deleteMany({});
  await db.files.deleteMany({});

  await organizationsSeeds();
  await eventsSeeds();
  await attendeesSeeds();
};

module.exports = seeds;
