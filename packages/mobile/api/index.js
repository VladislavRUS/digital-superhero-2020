import axios from 'axios';

const api = axios.create({
  baseURL: 'http://192.168.0.170:3001',
});

export const registerAttendee = (values) => api.post('/attendees', values);
export const loginAttendee = (values) => api.post('/attendees/login', values);
export const updateAttendee = (values) => api.post('/attendees/login', values);
export const getAttendee = (attendeeId) => api.get(`/attendees/${attendeeId}`);
export const setPushNotificationToken = (attendeeId, pushNotificationToken) =>
  api.put(`/attendees/${attendeeId}`, {
    pushNotificationToken,
  });

export const getEvents = () => api.get('/events');
export const participateEvent = (eventId, attendeeId) =>
  api.post(`/attendees/${attendeeId}/participate?eventId=${eventId}`);

export const registerVk = (code) => api.post(`/attendees/register-vk`, { code });
export const loginVk = (code) => api.post(`/attendees/login-vk`, { code });
