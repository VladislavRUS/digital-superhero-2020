import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';

const FullPageLoader = ({ isLoading }) => {
  return isLoading ? (
    <View style={styles.overlay}>
      <ActivityIndicator size="large" />
    </View>
  ) : null;
};

export { FullPageLoader };

const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
});
