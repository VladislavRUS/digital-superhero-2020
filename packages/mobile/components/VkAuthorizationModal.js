import React, { useEffect, useRef, useState } from 'react';
import { Modal, StyleSheet, SafeAreaView } from 'react-native';
import { WebView } from 'react-native-webview';
import queryString from 'query-string';
import { FullPageLoader } from './FullPageLoader';

const VkAuthorizationModal = ({ isOpened, onTokenRetrieve }) => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!isOpened && loading) {
      setLoading(false);
    } else if (isOpened && !loading) {
      setLoading(true);
    }
  }, [isOpened]);

  const onLoadEnd = () => {
    setLoading(false);
  };

  const onNavigationStateChange = (navigationState) => {
    const url = navigationState.url;

    if (url.indexOf('https://online.dshkazan.ru') === 0) {
      const questionMarkIndex = url.indexOf('?');

      const { code } = queryString.parse(url.slice(questionMarkIndex + 1));
      onTokenRetrieve(code);
    }
  };

  return (
    <Modal visible={isOpened} animationType="slide" style={styles.modal}>
      <SafeAreaView style={{ flex: 1 }}>
        {isOpened && (
          <WebView
            onNavigationStateChange={onNavigationStateChange}
            source={{
              uri: `https://oauth.vk.com/authorize?client_id=7604297&display=page&response_type=code&v=5.124&redirect_uri=https://online.dshkazan.ru&scope=email`,
            }}
            incognito={true}
            onLoadEnd={onLoadEnd}
          />
        )}
      </SafeAreaView>

      <FullPageLoader isLoading={loading} />
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    padding: 20,
  },
});

export default VkAuthorizationModal;
