import React from 'react';
import { TouchableWithoutFeedback, Text, Alert } from 'react-native';
import { Block, Icon } from 'galio-framework';
import { argonTheme } from '../constants';
import { useNavigation } from '@react-navigation/core';

const MenuItem = ({ item }) => {
  const navigation = useNavigation();

  const onPress = () => {
    if (item.disabled) {
      Alert.alert('Внимание', 'Для доступа к разделу необходимо оплатить участие');
    } else if (item.goTo) {
      navigation.navigate(item.goTo);
    }
  };

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Block
        style={{
          backgroundColor: 'rgba(0, 0, 0, 0.1)',
          width: '30%',
          alignItems: 'center',
          padding: 10,
          borderRadius: 3,
          elevation: 5,
          opacity: item.disabled ? 0.5 : 1,
        }}
      >
        <Block style={{ marginBottom: 10 }}>
          <Icon
            size={24}
            color={argonTheme.COLORS.WHITE}
            name={item.iconName}
            family={item.iconFamily || 'MaterialIcons'}
          />
        </Block>

        <Text style={{ fontSize: 12, textAlign: 'center', fontWeight: 'bold', color: '#fff' }}>{item.name}</Text>
      </Block>
    </TouchableWithoutFeedback>
  );
};

export { MenuItem };
