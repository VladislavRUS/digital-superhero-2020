import React from 'react';
import { Image } from 'react-native';

const ImageLogo = ({ source, size }) => {
  return <Image source={source} style={{ width: size, height: size }} resizeMode="contain" />;
};

export { ImageLogo };
