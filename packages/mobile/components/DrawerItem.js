import React from 'react';
import { StyleSheet, TouchableOpacity, Linking } from 'react-native';
import { Block, Text, theme } from 'galio-framework';

import Icon from './Icon';
import argonTheme from '../constants/Theme';
import { rootStore } from '../store/RootStore';

const SCREEN_NAMES = {
  Home: 'Мероприятия',
  Profile: 'Профиль',
  'Log out': 'Выйти',
};

const DrawerItem = ({ focused, title, navigation }) => {
  const renderIcon = () => {
    switch (title) {
      case 'Home':
        return (
          <Icon name="menu-8" family="ArgonExtra" size={14} color={focused ? 'white' : argonTheme.COLORS.PRIMARY} />
        );
      case 'Elements':
        return (
          <Icon name="map-big" family="ArgonExtra" size={14} color={focused ? 'white' : argonTheme.COLORS.ERROR} />
        );
      case 'Articles':
        return (
          <Icon name="spaceship" family="ArgonExtra" size={14} color={focused ? 'white' : argonTheme.COLORS.PRIMARY} />
        );
      case 'Profile':
        return (
          <Icon
            name="chart-pie-35"
            family="ArgonExtra"
            size={14}
            color={focused ? 'white' : argonTheme.COLORS.WARNING}
          />
        );
      case 'Account':
        return (
          <Icon name="calendar-date" family="ArgonExtra" size={14} color={focused ? 'white' : argonTheme.COLORS.INFO} />
        );
      case 'Getting Started':
        return <Icon name="spaceship" family="ArgonExtra" size={14} color={focused ? 'white' : 'rgba(0,0,0,0.5)'} />;
      case 'Log out':
        return <Icon name="nav-left" family="ArgonExtra" size={12} color={focused ? 'white' : 'rgba(0,0,0,0.5)'} />;
      default:
        return null;
    }
  };

  const containerStyles = [styles.defaultStyle, focused ? [styles.activeStyle, styles.shadow] : null];

  return (
    <TouchableOpacity
      style={{ height: 60 }}
      onPress={() => {
        if (title === 'Log out') {
          rootStore.attendeeStore.logout();
        } else {
          navigation.navigate(title);
        }
      }}
    >
      <Block flex row style={containerStyles}>
        <Block middle flex={0.1} style={{ marginRight: 5 }}>
          {renderIcon()}
        </Block>
        <Block row center flex={0.9}>
          <Text size={15} bold={focused ? true : false} color={focused ? 'white' : 'rgba(0,0,0,0.5)'}>
            {SCREEN_NAMES[title]}
          </Text>
        </Block>
      </Block>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  defaultStyle: {
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  activeStyle: {
    backgroundColor: argonTheme.COLORS.WARNING,
    borderRadius: 4,
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
  },
});

export default DrawerItem;
