import React from 'react';
import { withNavigation } from '@react-navigation/compat';
import PropTypes from 'prop-types';
import { StyleSheet, Text as RNText, Image, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import format from 'date-fns/format';
import { argonTheme } from '../constants';
import { rootStore } from '../store/RootStore';
import { ru } from 'date-fns/locale';

const EventCard = ({ navigation, event, cta, horizontal, full, style, ctaColor, imageStyle }) => {
  const imageStyles = [full ? styles.fullImage : styles.horizontalImage, imageStyle];
  const cardContainer = [styles.card, styles.shadow, style];
  const imgContainer = [
    styles.imageContainer,
    horizontal ? styles.horizontalStyles : styles.verticalStyles,
    styles.shadow,
  ];

  const showCost = horizontal && event.cost > 0;

  const { eventsStore } = rootStore;

  const onPress = () => {
    eventsStore.selectEvent(event);
    navigation.navigate('Event');
  };

  return (
    <Block row={horizontal} card flex style={cardContainer}>
      <TouchableWithoutFeedback onPress={onPress}>
        <Block flex style={imgContainer}>
          <Image source={{ uri: event.image }} style={imageStyles} resizeMode="cover" />
        </Block>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={onPress}>
        <Block flex style={styles.cardDescription}>
          {showCost && <Text style={styles.cost}>{event.cost} ₽</Text>}

          <RNText size={14} style={styles.cardTitle} numberOfLines={1}>
            {event.name}
          </RNText>

          <Text size={14} style={styles.date} muted>
            {format(new Date(event.startDate), 'dd.MM.yyyy', {
              locale: ru,
            })}{' '}
            -{' '}
            {format(new Date(event.endDate), 'dd.MM.yyyy', {
              locale: ru,
            })}
          </Text>

          {horizontal && (
            <Text size={14} style={styles.cardSubtitle} max>
              {event.description}
            </Text>
          )}
          <Text
            size={12}
            muted={!ctaColor}
            color={argonTheme.COLORS.WARNING}
            bold
            style={{ marginBottom: 6, marginTop: 'auto' }}
          >
            Подробнее
          </Text>
        </Block>
      </TouchableWithoutFeedback>
    </Block>
  );
};

EventCard.propTypes = {
  event: PropTypes.object,
  horizontal: PropTypes.bool,
  full: PropTypes.bool,
  ctaColor: PropTypes.string,
  cta: PropTypes.string,
  imageStyle: PropTypes.any,
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 0,
    minHeight: 114,
    marginBottom: 16,
    borderRadius: 5,
  },
  cardTitle: {
    flexWrap: 'wrap',
    fontWeight: '600',
  },
  cardSubtitle: {
    fontSize: 12,
    marginBottom: 10,
  },
  cardDescription: {
    padding: theme.SIZES.BASE / 2,
  },
  imageContainer: {
    borderRadius: 3,
    elevation: 1,
    overflow: 'hidden',
  },
  image: {
    // borderRadius: 3,
  },
  horizontalImage: {
    height: '100%',
    width: 'auto',
  },
  horizontalStyles: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
  verticalStyles: {
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    height: 120,
  },
  fullImage: {
    height: 215,
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 2,
  },
  cost: {
    position: 'absolute',
    right: 10,
    top: 10,
    fontSize: 9,
    color: '#fff',
    paddingVertical: 2,
    paddingHorizontal: 5,
    borderRadius: 4,
    overflow: 'hidden',
    fontWeight: '600',
    backgroundColor: argonTheme.COLORS.SUCCESS,
  },
  date: {
    fontSize: 12,
    marginBottom: 10,
    marginTop: 10,
  },
});

export default withNavigation(EventCard);
