import React, { useEffect } from 'react';
import { StyleSheet, Dimensions, ScrollView, RefreshControl, Image } from 'react-native';
import { Block, Text, theme } from 'galio-framework';

import { Card } from '../components';
import articles from '../constants/articles';
import { rootStore } from '../store/RootStore';
import EventCard from '../components/EventCard';
import { FullPageLoader } from '../components/FullPageLoader';
import { LoadingStates } from '../constants/LoadingStates';
import { observer } from 'mobx-react';
import Images from '../constants/Images';
import { argonTheme } from '../constants';
const { width, height } = Dimensions.get('screen');

const Home = () => {
  const { eventsStore, attendeeStore } = rootStore;
  const { attendee } = attendeeStore;

  useEffect(() => {
    eventsStore.fetchEvents();
  }, []);

  const { filteredEvents } = eventsStore;

  const participatingEvents = filteredEvents.filter((event) => attendee.activeEventsIds.indexOf(event.id) !== -1);
  const otherEvents = filteredEvents.filter((event) => attendee.activeEventsIds.indexOf(event.id) === -1);

  const renderEvents = () => {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.articles}
        refreshControl={
          <RefreshControl
            refreshing={eventsStore.eventsLoadingState === LoadingStates.PENDING}
            onRefresh={() => eventsStore.fetchEvents()}
          />
        }
      >
        {participatingEvents.length > 0 && (
          <Block>
            <Block row style={{ alignItems: 'center', paddingHorizontal: 15 }}>
              <Text bold size={16}>
                Мои мероприятия
              </Text>

              <Block flex right>
                <Text size={12} style={{ fontWeight: 'bold' }} color={argonTheme.COLORS.WARNING}>
                  Смотреть все
                </Text>
              </Block>
            </Block>

            <ScrollView
              horizontal
              contentContainerStyle={{ paddingHorizontal: 15 }}
              showsHorizontalScrollIndicator={false}
            >
              {participatingEvents.map((event, idx) => (
                <Block
                  style={{ width: 165, marginRight: idx === participatingEvents.length - 1 ? 0 : theme.SIZES.BASE }}
                >
                  <EventCard event={event} />
                </Block>
              ))}
            </ScrollView>
          </Block>
        )}

        <Block style={{ paddingHorizontal: 15 }}>
          <Block row style={{ alignItems: 'center', marginTop: 15 }}>
            <Text bold size={16}>
              Мероприятия на карте
            </Text>
            <Block flex right>
              <Text size={12} style={{ fontWeight: 'bold' }} color={argonTheme.COLORS.WARNING}>
                Смотреть все
              </Text>
            </Block>
          </Block>

          <Block style={{ marginTop: 15, marginBottom: 15 }}>
            <Image source={Images.Map} style={{ height: 150, width: '100%', borderRadius: 5 }} resizeMode="cover" />
          </Block>
        </Block>

        {otherEvents.length > 0 && (
          <Block style={{ paddingHorizontal: 15 }}>
            <Block row style={{ alignItems: 'center', marginTop: 15 }}>
              <Text bold size={16}>
                Ближайшие мероприятия
              </Text>

              <Block flex right>
                <Text size={12} style={{ fontWeight: 'bold' }} color={argonTheme.COLORS.WARNING}>
                  Смотреть все
                </Text>
              </Block>
            </Block>

            {otherEvents.map((event) => (
              <EventCard event={event} horizontal />
            ))}
          </Block>
        )}
      </ScrollView>
    );
  };

  return (
    <Block flex style={styles.home}>
      <Block flex center>
        {renderEvents()}
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width,
    paddingVertical: theme.SIZES.BASE,
  },
});

export default observer(Home);
