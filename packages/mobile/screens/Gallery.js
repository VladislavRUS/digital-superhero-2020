import React from 'react';
import { ScrollView, Image } from 'react-native';
import Images from '../constants/Images';
import { ImageBackground } from 'react-native';
import { Block, Text } from 'galio-framework';

const images = [
  {
    uri: 'https://funformula.one/wp-content/uploads/2020/08/image-3.jpg',
    hashtags: '#формула1 #машины #вождение',
  },
  {
    uri: 'https://sites.google.com/site/volejbol06/_/rsrc/1370873388691/home/g40_296.jpeg?height=240&width=320',
    hashtags: '#волейбол #мяч #соревнования',
  },
  {
    uri: 'https://like2play.ru/wp-content/uploads/2017/05/%D0%BA%D0%B0%D0%BD%D0%B0%D1%82.jpg',
    hashtags: '#канаты',
  },
];

const Gallery = () => {
  return (
    <ImageBackground source={Images.Onboarding} style={{ width: '100%', height: '100%' }}>
      <ScrollView
        style={{ flex: 1, marginTop: 100 }}
        contentContainerStyle={{ paddingVertical: 20, paddingHorizontal: 10 }}
      >
        {images.map((image) => (
          <Block
            style={{
              width: '100%',
              height: 200,
              backgroundColor: '#fff',
              borderRadius: 3,
              overflow: 'hidden',
              marginBottom: 10,
            }}
          >
            <Image source={{ uri: image.uri }} style={{ height: 150, width: '100%' }} />

            <Text muted style={{ padding: 5, marginTop: 10, marginLeft: 5 }}>
              {image.hashtags}
            </Text>
          </Block>
        ))}
      </ScrollView>
    </ImageBackground>
  );
};

export default Gallery;
