import React from 'react';
import { ScrollView, Alert, StatusBar, ImageBackground, Share } from 'react-native';
import { observer } from 'mobx-react';
import { rootStore } from '../store/RootStore';
import { Block, Icon, Text, theme } from 'galio-framework';
import { argonTheme } from '../constants';
import Images from '../constants/Images';
import Button from '../components/Button';
import { MenuItem } from '../components/MenuItem';
import { FullPageLoader } from '../components/FullPageLoader';
import { LoadingStates } from '../constants/LoadingStates';

const Event = () => {
  const { eventsStore, attendeeStore } = rootStore;

  const { attendee } = attendeeStore;
  const { selectedEvent } = eventsStore;

  if (!selectedEvent || !attendee) {
    return null;
  }

  const isParticipant = attendee.activeEventsIds.indexOf(selectedEvent.id) !== -1;

  const onParticipate = () => {
    if (selectedEvent.cost > 0) {
      Alert.alert('Внимание', `Стоимость участия в мероприятии состоявляет ${selectedEvent.cost} рублей`, [
        {
          text: 'Отмена',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Оплатить',
          onPress: () => {
            attendeeStore.participateEvent(selectedEvent);
          },
        },
      ]);
    } else {
      attendeeStore.participateEvent(selectedEvent);
    }
  };

  const onShare = async () => {
    try {
      await Share.share({
        message: `Интересное мероприятие - ${selectedEvent.name}`,
      });
    } catch (e) {}
  };

  const disableMenuitem = selectedEvent.cost > 0 && !isParticipant;

  const menuItems = [
    {
      name: 'Общая информация',
      iconName: 'info',
      goTo: 'CommonInformation',
    },
    {
      name: 'Как добраться',
      iconName: 'map',
    },
    {
      name: 'Турнирная таблица',
      iconName: 'table',
      iconFamily: 'AntDesign',
      disabled: disableMenuitem,
    },
    {
      name: 'Онлайн трансляция',
      iconName: 'videocamera',
      iconFamily: 'AntDesign',
      disabled: disableMenuitem,
      goTo: 'Stream',
    },
    {
      name: 'Новости',
      iconName: 'rss',
      iconFamily: 'Entypo',
      disabled: disableMenuitem,
    },
    {
      name: 'Галерея',
      iconName: 'image',
      disabled: disableMenuitem,
      goTo: 'Gallery',
    },
    {
      name: 'Общение',
      iconName: 'chat',
      iconFamily: 'Entypo',
      disabled: disableMenuitem,
    },
    {
      name: 'Спонсоры и партнеры',
      iconName: 'attach-money',
    },
    {
      name: 'FAQ',
      iconName: 'questioncircle',
      iconFamily: 'AntDesign',
    },
  ];

  return (
    <ImageBackground style={{ flex: 1, position: 'relative' }} source={Images.Onboarding}>
      <StatusBar hidden />
      <ScrollView contentContainerStyle={{ flex: 1, paddingBottom: 50, justifyContent: 'center' }}>
        <StatusBar hidden />

        <Block style={{ padding: 20, position: 'absolute', top: 31, left: 30 }}>
          <Text h5 style={{ fontWeight: '700' }} color={argonTheme.COLORS.WHITE}>
            {selectedEvent.name}
          </Text>
        </Block>

        <Block style={{ padding: 20 }}>
          <Block row space="between" style={{ marginBottom: 20 }}>
            {menuItems.slice(0, 3).map((item, idx) => (
              <MenuItem key={idx} item={item} />
            ))}
          </Block>

          <Block row space="between" style={{ marginBottom: 20 }}>
            {menuItems.slice(3, 6).map((item, idx) => (
              <MenuItem key={idx} item={item} />
            ))}
          </Block>
          <Block row space="between">
            {menuItems.slice(6, 9).map((item, idx) => (
              <MenuItem key={idx} item={item} />
            ))}
          </Block>
        </Block>
      </ScrollView>

      <Block style={{ position: 'absolute', left: 0, bottom: 0, right: 0, padding: 20 }}>
        <Button color="transparent" onPress={onShare}>
          <Block row style={{ alignItems: 'center' }}>
            <Icon name="share" family="FontAwesome" color="#fff" size={14} />
            <Text style={{ color: '#fff', fontSize: 12, fontWeight: '700', marginLeft: 10 }}>
              Поделиться с друзьями
            </Text>
          </Block>
        </Button>

        {!isParticipant && (
          <Button
            color="secondary"
            textStyle={{ color: 'black', fontSize: 12, fontWeight: '700' }}
            onPress={onParticipate}
            style={{ marginTop: 10 }}
          >
            {selectedEvent.cost > 0 ? 'Оплатить и принять участие' : 'Принять участие'}
          </Button>
        )}
      </Block>

      <FullPageLoader isLoading={attendeeStore.participateLoadingState === LoadingStates.PENDING} />
    </ImageBackground>
  );
};

export default observer(Event);
