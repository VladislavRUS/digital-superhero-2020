import React, { useState } from 'react';
import { StyleSheet, ImageBackground, Dimensions, StatusBar, KeyboardAvoidingView } from 'react-native';
import { Block, Checkbox, Text, theme } from 'galio-framework';
import { Button, Icon, Input } from '../components';
import { Images, argonTheme } from '../constants';
import { ImageLogo } from '../components/Imagelogo';
import { observer } from 'mobx-react';
import { rootStore } from '../store/RootStore';
import { FullPageLoader } from '../components/FullPageLoader';
import { LoadingStates } from '../constants/LoadingStates';
import VkAuthorizationModal from '../components/VkAuthorizationModal';

const { width, height } = Dimensions.get('screen');

const Register = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [policyAccepted, setPolicyAccepted] = useState(false);
  const [isVkOpened, setVkOpened] = useState(false);

  const { attendeeStore } = rootStore;

  const onRegister = () => {
    attendeeStore.register({ firstName, lastName, email, password });
  };

  const onTokenRetrieve = (code) => {
    setVkOpened(false);
    attendeeStore.registerVk(code);
  };

  return (
    <Block flex middle>
      <StatusBar hidden />
      <ImageBackground source={Images.Onboarding} style={{ width, height, zIndex: 1 }}>
        <Block flex middle>
          <Block style={styles.registerContainer}>
            <Block flex={0.25} middle style={styles.socialConnect}>
              <Text color="#8898AA" size={12}>
                Создать аккаунт с помощью
              </Text>
              <Block row style={{ marginTop: theme.SIZES.BASE }}>
                <Button style={{ ...styles.socialButtons, marginRight: 30 }} onPress={() => setVkOpened(true)}>
                  <Block row style={{ alignItems: 'center' }}>
                    <Block style={{ marginRight: 5 }}>
                      <ImageLogo source={require('../assets/imgs/vk-logo.png')} size={12} />
                    </Block>

                    <Text style={styles.socialTextButtons}>ВКонтакте</Text>
                  </Block>
                </Button>
                <Button style={styles.socialButtons}>
                  <Block row>
                    <Block style={{ marginRight: 5 }}>
                      <ImageLogo source={require('../assets/imgs/gos-logo.png')} size={14} />
                    </Block>

                    <Text style={styles.socialTextButtons}>Госуслуги</Text>
                  </Block>
                </Button>
              </Block>
            </Block>

            <Block flex>
              <Block middle style={{ marginTop: 30, marginBottom: 15 }}>
                <Text color="#8898AA" size={12}>
                  Регистрация
                </Text>
              </Block>

              <Block flex center>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled>
                  <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                    <Input
                      value={firstName}
                      onChangeText={setFirstName}
                      borderless
                      placeholder="Имя"
                      iconContent={
                        <Icon
                          size={16}
                          color={argonTheme.COLORS.ICON}
                          name="user"
                          family="Entypo"
                          style={styles.inputIcons}
                        />
                      }
                    />
                  </Block>

                  <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                    <Input
                      value={lastName}
                      onChangeText={setLastName}
                      borderless
                      placeholder="Фамилия"
                      iconContent={
                        <Icon
                          size={16}
                          color={argonTheme.COLORS.ICON}
                          name="user"
                          family="Entypo"
                          style={styles.inputIcons}
                        />
                      }
                    />
                  </Block>

                  <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                    <Input
                      value={email}
                      onChangeText={setEmail}
                      borderless
                      placeholder="Почта"
                      iconContent={
                        <Icon
                          size={16}
                          color={argonTheme.COLORS.ICON}
                          name="mail"
                          family="Ionicons"
                          style={styles.inputIcons}
                        />
                      }
                    />
                  </Block>

                  <Block width={width * 0.8}>
                    <Input
                      value={password}
                      onChangeText={setPassword}
                      password
                      borderless
                      placeholder="Пароль"
                      iconContent={
                        <Icon
                          size={16}
                          color={argonTheme.COLORS.ICON}
                          name="padlock-unlocked"
                          family="ArgonExtra"
                          style={styles.inputIcons}
                        />
                      }
                    />
                  </Block>
                  <Block row width={width * 0.75} style={{ marginTop: 10 }}>
                    <Checkbox
                      onChange={(checked) => setPolicyAccepted(checked)}
                      checkboxStyle={{
                        borderWidth: 3,
                      }}
                      color={argonTheme.COLORS.PRIMARY}
                      label="Я согласен с"
                    />
                    <Button
                      style={{ width: 190 }}
                      color="transparent"
                      textStyle={{
                        color: argonTheme.COLORS.PRIMARY,
                        fontSize: 14,
                      }}
                    >
                      Политикой использования
                    </Button>
                  </Block>
                  <Block middle flex={0.5}>
                    <Button color="warning" style={styles.createButton} onPress={onRegister} disabled={!policyAccepted}>
                      <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                        РЕГИСТРАЦИЯ
                      </Text>
                    </Button>
                  </Block>
                </KeyboardAvoidingView>
              </Block>
            </Block>
          </Block>
        </Block>

        <VkAuthorizationModal isOpened={isVkOpened} onTokenRetrieve={onTokenRetrieve} />

        <FullPageLoader isLoading={attendeeStore.registerLoadingState === LoadingStates.PENDING} />
      </ImageBackground>
    </Block>
  );
};

const styles = StyleSheet.create({
  registerContainer: {
    width: width * 0.9,
    height: height * 0.9,
    justifyContent: 'center',
    backgroundColor: '#F4F5F7',
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: 'hidden',
  },
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: '#8898AA',
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: '#fff',
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
  },
  socialTextButtons: {
    color: argonTheme.COLORS.BLACK,
    fontWeight: '400',
    fontSize: 12,
  },
  inputIcons: {
    marginRight: 12,
  },
  passwordCheck: {
    paddingLeft: 15,
    paddingTop: 13,
    paddingBottom: 30,
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25,
  },
});

export default observer(Register);
