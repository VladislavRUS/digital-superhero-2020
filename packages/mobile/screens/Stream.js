import React, { useState } from 'react';
import { ImageBackground } from 'react-native';
import { Block } from 'galio-framework';
import Images from '../constants/Images';
import { WebView } from 'react-native-webview';
import { rootStore } from '../store/RootStore';
import { FullPageLoader } from '../components/FullPageLoader';

const Stream = () => {
  const { eventsStore } = rootStore;
  const { selectedEvent } = eventsStore;

  const [loading, setLoading] = useState(!!selectedEvent.stream);

  return (
    <ImageBackground source={Images.Onboarding} style={{ width: '100%', height: '100%' }}>
      <Block flex style={{ alignItems: 'center', justifyContent: 'center' }}>
        <Block style={{ backgroundColor: 'transparent', height: 400, width: '100%', position: 'relative' }}>
          {selectedEvent.stream && <WebView source={{ uri: selectedEvent.stream }} onLoad={() => setLoading(false)} />}
        </Block>
      </Block>
      <FullPageLoader isLoading={loading} />
    </ImageBackground>
  );
};

export default Stream;
