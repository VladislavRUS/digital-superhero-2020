import React from 'react';
import { Image, ScrollView } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import { rootStore } from '../store/RootStore';
import { argonTheme } from '../constants';
import format from 'date-fns/format';
import { ru } from 'date-fns/locale';

const CommonInformation = () => {
  const { eventsStore } = rootStore;

  const { selectedEvent } = eventsStore;

  return (
    <Block flex>
      <ScrollView contentContainerStyle={{ flex: 1 }}>
        <Image source={{ uri: selectedEvent.image }} style={{ width: '100%', height: 200 }} />
        <Block style={{ padding: 20 }}>
          <Text h3 color={argonTheme.COLORS.DEFAULT} style={{ fontWeight: 'bold' }}>
            {selectedEvent.name}
          </Text>
        </Block>
        <Block style={{ paddingHorizontal: 20, marginBottom: 20 }}>
          <Text h5 color={argonTheme.COLORS.DEFAULT}>
            {format(new Date(selectedEvent.startDate), 'dd MMM, yyyy', {
              locale: ru,
            })}{' '}
            -{' '}
            {format(new Date(selectedEvent.endDate), 'dd MMM, yyyy', {
              locale: ru,
            })}
          </Text>
        </Block>
        <Block style={{ paddingHorizontal: 20 }}>
          <Text muted color={argonTheme.COLORS.DEFAULT}>
            {selectedEvent.description}
          </Text>
        </Block>
      </ScrollView>
    </Block>
  );
};

export default CommonInformation;
