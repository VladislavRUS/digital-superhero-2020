// local imgs
const Onboarding = require('../assets/imgs/bg.png');
const Logo = require('../assets/imgs/logo.png');
const LogoSmall = require('../assets/imgs/logo_small.png');
const LogoOnboarding = require('../assets/imgs/logo-white.png');
const ProfileBackground = require('../assets/imgs/profile-screen-bg.png');
const RegisterBackground = require('../assets/imgs/register-bg.png');
const Pro = require('../assets/imgs/getPro-bg.png');
const ArgonLogo = require('../assets/imgs/argonlogo.png');
const iOSLogo = require('../assets/imgs/ios.png');
const Map = require('../assets/imgs/map.png');
const androidLogo = require('../assets/imgs/android.png');
// internet imgs

const ProfilePicture = 'https://images.unsplash.com/photo-1492633423870-43d1cd2775eb?fit=crop&w=1650&q=80';

const Viewed = [
  'https://ss.sport-express.ru/userfiles/materials/159/1598621/origin_82355680.jpg',
  'https://1prof.by/storage/2020/03/ran-0303.jpg',
  'https://nevasport.ru/wp-content/uploads/2018/10/Snimok-17-1024x566.jpg',
  'https://cdn.pixabay.com/photo/2018/07/24/13/46/sport-3559216_1280.jpg',
  'https://i.trbna.com/preset/wysiwyg/9/92/52e58e48011ea896393d57216d353.jpeg',
  'https://zelsport.ru/uploads/default/files/421d2ff58f299961730900a62f402fda.jpg',
];

const Products = {
  'View article':
    'https://images.unsplash.com/photo-1501601983405-7c7cabaa1581?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=840&q=840',
};

export default {
  Onboarding,
  Logo,
  LogoOnboarding,
  ProfileBackground,
  ProfilePicture,
  RegisterBackground,
  Viewed,
  Products,
  Pro,
  ArgonLogo,
  iOSLogo,
  androidLogo,
  LogoSmall,
  Map,
};
