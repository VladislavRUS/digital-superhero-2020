const LoadingStates = {
  IDLE: 'idle',
  PENDING: 'pending',
  DONE: 'done',
  ERROR: 'error',
};

export { LoadingStates };
