import { action, computed, observable, runInAction } from 'mobx';
import { LoadingStates } from '../constants/LoadingStates';
import { getEvents, participateEvent } from '../api';

class EventsStore {
  rootStore;
  @observable events = [];
  @observable selectedEventId;
  @observable eventsLoadingState = LoadingStates.IDLE;

  constructor(rootStore) {
    this.rootStore = rootStore;
  }

  @computed
  get filteredEvents() {
    return this.events
      .filter((event) => event.approved)
      .sort((first, second) => {
        return new Date(first.startDate).getTime() - new Date(second.startDate).getTime();
      });
  }

  @computed
  get selectedEvent() {
    return this.filteredEvents.find((event) => event.id === this.selectedEventId);
  }

  @action
  async fetchEvents() {
    this.eventsLoadingState = LoadingStates.PENDING;

    try {
      const { data } = await getEvents();

      runInAction(() => {
        this.events = data;
        this.eventsLoadingState = LoadingStates.DONE;
      });
    } catch (e) {
      runInAction(() => {
        this.eventsLoadingState = LoadingStates.ERROR;
      });
    }
  }

  @action
  selectEvent(event) {
    this.selectedEventId = event.id;
  }
}

export default EventsStore;
