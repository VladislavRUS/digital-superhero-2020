import AttendeeStore from './AttendeeStore';
import EventsStore from './EventsStore';

class RootStore {
  constructor() {
    this.attendeeStore = new AttendeeStore(this);
    this.eventsStore = new EventsStore(this);
  }
}

const rootStore = new RootStore();

export { rootStore };
