import { action, runInAction, observable } from 'mobx';
import { LoadingStates } from '../constants/LoadingStates';
import {
  getAttendee,
  loginAttendee,
  loginVk,
  participateEvent,
  registerAttendee,
  registerVk,
  setPushNotificationToken,
} from '../api';
import AsyncStorage from '@react-native-community/async-storage';
import { Alert } from 'react-native';

class AttendeeStore {
  rootStore;
  @observable attendee;
  @observable loginLoadingState = LoadingStates.IDLE;
  @observable registerLoadingState = LoadingStates.IDLE;
  @observable participateLoadingState = LoadingStates.IDLE;
  @observable refreshLoadingState = LoadingStates.IDLE;
  @observable vkRegisterLoadingState = LoadingStates.IDLE;
  @observable vkLoginLoadingState = LoadingStates.IDLE;
  @observable setPushNotificationTokenLoadingState = LoadingStates.IDLE;

  constructor(rootStore) {
    this.rootStore = rootStore;

    this.initAttendee();
  }

  async initAttendee() {
    const attendee = await AsyncStorage.getItem('attendee');

    if (attendee) {
      this.attendee = JSON.parse(attendee);
    }
  }

  @action
  async refreshAttendee() {
    this.refreshLoadingState = LoadingStates.PENDING;

    try {
      const { data } = await getAttendee(this.attendee.id);

      await AsyncStorage.setItem('attendee', JSON.stringify(data));

      runInAction(() => {
        this.attendee = data;
        this.refreshLoadingState = LoadingStates.DONE;
      });
    } catch (e) {
      runInAction(() => {
        this.refreshLoadingState = LoadingStates.ERROR;
      });
    }
  }

  @action
  async login(values) {
    this.loginLoadingState = LoadingStates.PENDING;

    try {
      const { data } = await loginAttendee(values);

      await AsyncStorage.setItem('attendee', JSON.stringify(data));

      runInAction(() => {
        this.attendee = data;
        this.loginLoadingState = LoadingStates.DONE;
      });
    } catch (e) {
      alert('Не удалось авторизоваться!');
      runInAction(() => {
        this.loginLoadingState = LoadingStates.ERROR;
      });
    }
  }

  @action
  async register(values) {
    this.registerLoadingState = LoadingStates.PENDING;

    try {
      const { data } = await registerAttendee(values);

      await AsyncStorage.setItem('attendee', JSON.stringify(data));

      runInAction(() => {
        this.attendee = data;
        this.registerLoadingState = LoadingStates.DONE;
      });
    } catch (e) {
      alert('Не удалось зарегистрироваться!');

      runInAction(() => {
        this.registerLoadingState = LoadingStates.ERROR;
      });
    }
  }

  @action
  async setPushNotificationToken(token) {
    this.setPushNotificationTokenLoadingState = LoadingStates.PENDING;

    try {
      await setPushNotificationToken(this.attendee.id, token);

      runInAction(() => {
        this.setPushNotificationTokenLoadingState = LoadingStates.DONE;
      });
    } catch (e) {
      runInAction(() => {
        this.setPushNotificationTokenLoadingState = LoadingStates.ERROR;
      });
    }
  }

  @action
  async logout() {
    await AsyncStorage.removeItem('attendee');
    this.attendee = null;
  }

  @action
  async participateEvent(event) {
    this.participateLoadingState = LoadingStates.PENDING;

    try {
      const { data } = await participateEvent(event.id, this.attendee.id);

      runInAction(() => {
        this.attendee = data;
        this.participateLoadingState = LoadingStates.DONE;
      });

      Alert.alert('Поздравляем', 'Вы успешно зарегистрировались в качестве участника!');
    } catch (e) {
      alert('Не удалось принять участие!');
      runInAction(() => {
        this.participateLoadingState = LoadingStates.ERROR;
      });
    }
  }

  @action
  async loginVk(code) {
    this.vkLoginLoadingState = LoadingStates.PENDING;

    try {
      const { data } = await loginVk(code);

      runInAction(() => {
        this.attendee = data;
        this.vkLoginLoadingState = LoadingStates.DONE;
      });
    } catch (e) {
      runInAction(() => {
        this.vkLoginLoadingState = LoadingStates.ERROR;
      });
    }
  }

  @action
  async registerVk(code) {
    this.vkRegisterLoadingState = LoadingStates.PENDING;

    try {
      const { data } = await registerVk(code);

      runInAction(() => {
        this.attendee = data;
        this.vkRegisterLoadingState = LoadingStates.DONE;
      });

      Alert.alert('Поздравляем', 'Вы успешно зарегистрировались в качестве участника!');
    } catch (e) {
      runInAction(() => {
        this.vkRegisterLoadingState = LoadingStates.ERROR;
      });
    }
  }
}

export default AttendeeStore;
