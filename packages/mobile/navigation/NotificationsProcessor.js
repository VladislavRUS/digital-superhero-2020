import React, { useEffect } from 'react';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import { rootStore } from '../store/RootStore';

const registerForPushNotificationsAsync = async () => {
  if (Constants.isDevice) {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      return;
    }

    return await Notifications.getExpoPushTokenAsync();
  }
};

const NotificationsProcessor = () => {
  const { attendeeStore } = rootStore;

  const { attendee } = attendeeStore;

  useEffect(() => {
    async function prepareNotifications() {
      const token = await registerForPushNotificationsAsync();

      if (token) {
        await attendeeStore.setPushNotificationToken(token);
      }
    }

    if (attendee) {
      prepareNotifications();
    }
  }, [attendee]);
  return null;
};

export { NotificationsProcessor };
